package edu.passau.uni.de.letsgobuddy.firebase.listeners.singleEventListeners;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import edu.passau.uni.de.letsgobuddy.domain.activities.CreateAppointmentActivity;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * {@link FirebaseValueListener} to load the {@link Group} for creating a new {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment}.
 */
public class CreateAppointmentGroupLoadListener extends FirebaseValueListener {


    /**
     * {@link java.lang.reflect.Constructor} for a {@link CreateAppointmentGroupLoadListener}.
     *
     * @param activity  the {@link BaseActivity}.
     * @param reference the {@link DatabaseReference}.
     */
    public CreateAppointmentGroupLoadListener(BaseActivity activity, DatabaseReference reference) {
        super(reference, true, activity);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        // get group and set in activity
        Group group = dataSnapshot.getValue(Group.class);
        if (group != null) {
            ((CreateAppointmentActivity) activity).setGroup(group);
        }
        activity.hideLoadingScreen();
    }
}
