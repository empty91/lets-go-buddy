package edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import edu.passau.uni.de.letsgobuddy.domain.activities.ExpiredAppointmentsActivity;
import edu.passau.uni.de.letsgobuddy.domain.activities.GroupViewActivity;
import edu.passau.uni.de.letsgobuddy.domain.adapters.AppointmentListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


/**
 * {@link FirebaseValueListener} to listen on {@link Appointment appointments} of a {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Group}.
 */
public class AppointmentLoadListener extends FirebaseValueListener {

    private List<Appointment> appointments;
    private AppointmentListAdapter appointmentListAdapter;

    /**
     * {@link java.lang.reflect.Constructor} for an {@link AppointmentLoadListener}
     *
     * @param appointments           the {@link List} of {@link Appointment appointments} to fill.
     * @param appointmentListAdapter the {@link AppointmentListAdapter} to notifiy.
     * @param activity               the {@link BaseActivity} the {@link AppointmentLoadListener} is registered to.
     * @param reference              the {@link com.google.firebase.database.DatabaseReference}.
     */
    public AppointmentLoadListener(List<Appointment> appointments, AppointmentListAdapter appointmentListAdapter, BaseActivity activity, Query reference) {
        super(reference, activity);
        this.appointments = appointments;
        this.appointmentListAdapter = appointmentListAdapter;
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        activity.showLoadingScreen();
        appointments.clear();
        Date currentDate = new Date();

        for (DataSnapshot child : dataSnapshot.getChildren()) {
            Appointment appointment = child.getValue(Appointment.class);
            if (appointment != null) {
                Date appointmentDate = new Date(appointment.getDateAndTime());
                if (activity instanceof GroupViewActivity) {
                    if (appointmentDate.after(currentDate)) {
                        appointments.add(appointment);
                    }
                }
                if (activity instanceof ExpiredAppointmentsActivity) {
                    if (appointmentDate.before(currentDate)) {
                        appointments.add(appointment);
                    }
                }
            }
        }

        // sort by date
        Collections.sort(appointments);
        appointmentListAdapter.notifyDataSetChanged();
        activity.hideLoadingScreen();
    }

}

