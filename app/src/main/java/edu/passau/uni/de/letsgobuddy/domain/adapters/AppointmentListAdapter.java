package edu.passau.uni.de.letsgobuddy.domain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.util.DateFormatUtil;

/**
 * {@link ArrayAdapter} to display a {@link List} of {@link Appointment appointments}.
 */
public class AppointmentListAdapter extends ArrayAdapter<Appointment> {

    private Group group;
    private List<Appointment> appointments;

    /**
     * {@link java.lang.reflect.Constructor} for an {@link AppointmentListAdapter}.
     *
     * @param context      the {@link Context}
     * @param group        the {@link Group}
     * @param appointments the {@link Appointment appointments}.
     */
    public AppointmentListAdapter(@NonNull Context context, @NonNull Group group, @NonNull List<Appointment> appointments) {
        super(context, R.layout.groupview_appointment_items_fragment, appointments);
        this.group = group;
        this.appointments = appointments;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi != null ? vi.inflate(R.layout.groupview_appointment_items_fragment, null) : null;
        }

        if (convertView != null) {

            Appointment appointment = appointments.get(position);
            if (appointment != null) {

                TextView appointmentName = convertView.findViewById(R.id.appointment_item_name);
                TextView appointmentDate = convertView.findViewById(R.id.appointment_item_date);
                appointmentName.setText(appointment.getName());
                Calendar c = new GregorianCalendar();
                c.setTime(new Date(appointment.getDateAndTime()));
                String dateAndTime = DateFormatUtil.formatDate(getContext(), c, DateFormat.SHORT) + " " + DateFormatUtil.formatTime(c);
                appointmentDate.setText(dateAndTime);
            }
        }

        return convertView;

    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}

