package edu.passau.uni.de.letsgobuddy.domain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.LinkedList;
import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.adapters.AppointmentListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.AppointmentLoadListener;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.GroupLoadListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * {@link android.app.Activity} to display the expired {@link Appointment appointments}.
 */
public class ExpiredAppointmentsActivity extends BaseActivity {

    private Group group = new Group();
    private AppointmentListAdapter appointmentListAdapter;
    private List<Appointment> appointments = new LinkedList<>();
    private String groupID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.expired_appointments_activity);

        prepareUI();
        prepareContent();
    }

    /**
     * Prepares the UI.
     */
    private void prepareUI() {

        // init list and listViewAdapter
        appointmentListAdapter = new AppointmentListAdapter(ExpiredAppointmentsActivity.this, group, appointments);
        ListView appointmentView = findViewById(R.id.expired_appointment_list);
        if (appointmentView != null) {
            appointmentView.setAdapter(appointmentListAdapter);
        }

        appointmentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String appointmentUid = appointments.get(position).getUid();
                Intent intent = new Intent(ExpiredAppointmentsActivity.this, AppointmentInfoActivity.class);
                intent.putExtra(Constants.APPOINTMENT_ID, appointmentUid);
                intent.putExtra(Constants.GROUP_ID, groupID);
                intent.putExtra(Constants.ADMIN, isAdmin);
                intent.putExtra(Constants.EXPIRED, true);
                startActivity(intent);
            }
        });
    }

    /**
     * Prepares the Content.
     */
    private void prepareContent() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            groupID = String.valueOf(extras.get(Constants.GROUP_ID));
            if (groupID != null) {

                // load Group
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID);
                new GroupLoadListener(group, this, reference);

                // load appointments
                Query appointmentsRef = FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).orderByChild(Constants.GROUP_ID).equalTo(groupID);
                new AppointmentLoadListener(appointments, appointmentListAdapter, this, appointmentsRef);

            }
        }
    }


    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        restart();
        prepareUI();
        prepareContent();
    }

    private void restart() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null && group != null) {
            boolean isAdmin = group.isAdmin(currentUser.getUid());
            setAdmin(isAdmin);
            onCreateOptionsMenu(this.menu);
        }
        appointments.clear();
    }

}

