package edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.passau.uni.de.letsgobuddy.domain.activities.GroupInfoActivity;
import edu.passau.uni.de.letsgobuddy.domain.adapters.UserListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


/**
 * {@link FirebaseValueListener} to listen to a {@link Group} and its {@link User users}.
 */
public class GroupInfoListener extends FirebaseValueListener {

    private Group group;
    private List<User> users;
    private Map<String, Integer> userRegister = new HashMap<>();
    private UserListAdapter userListAdapter;

    /**
     * {@link java.lang.reflect.Constructor} for a {@link GroupInfoListener}.
     *
     * @param group           the {@link Group} to listen on.
     * @param users           the {@link User users} of the {@link Group}.
     * @param userListAdapter the {@link UserListAdapter} to notify.
     * @param activity        the {@link BaseActivity} the {@link GroupInfoListener} is registered to.
     * @param reference       the {@link DatabaseReference}.
     */
    public GroupInfoListener(Group group, List<User> users, UserListAdapter userListAdapter, BaseActivity activity, DatabaseReference reference) {
        super(reference, activity);
        this.group = group;
        this.users = users;
        this.userListAdapter = userListAdapter;
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        activity.showLoadingScreen();
        group = dataSnapshot.getValue(Group.class);
        if (group != null) {
            if (activity instanceof GroupInfoActivity) {
                userListAdapter.setGroup(group);
                activity.setAdmin(group.isAdmin(FirebaseAuth.getInstance().getCurrentUser().getUid()));
                ((GroupInfoActivity) activity).setGroup(group);

                // iterate over group.getUsers
                for (String userUid : group.getUsers().values()) {
                    Integer index = userRegister.get(userUid); // get index of user
                    User user = new User(userUid);
                    if (index != null && index < group.getUsers().values().size()) { // user exists and not out of bounds
                        users.remove(index.intValue());
                        users.add(index, user); // reinsert user
                    } else { // user not exists yet, so insert
                        users.add(user);
                        userRegister.put(userUid, users.indexOf(user));
                    }
                }

                if (activity.getSupportActionBar() != null) { // set toolbar title
                    activity.getSupportActionBar().setTitle(group.getName());
                }
                cleanRegister();
                userListAdapter.notifyDataSetChanged();
            }
        }
        activity.hideLoadingScreen();
    }

    /**
     * Cleans the userregister to make sure there are no duplicate entries of wrong indizes.
     */
    private void cleanRegister() {

        // keys to remove from the userRegister
        List<String> userRegisterRemove = new LinkedList<>();

        // collect all keys to remove
        for (String userRegisterKey : userRegister.keySet()) {
            if (!group.getUsers().values().contains(userRegisterKey)) {
                userRegisterRemove.add(userRegisterKey);
            }
        }

        List<User> usersRemove = new LinkedList<>();

        for (String userRegisterRemoveKey : userRegisterRemove) {

            // remove from register
            userRegister.remove(userRegisterRemoveKey);
            for (User user : users) { // collect users to remove
                if (user.getUid().equals(userRegisterRemoveKey)) {
                    usersRemove.add(user);
                }
            }
        }

        userRegister.clear();
        users.removeAll(usersRemove);
        for (User user : users) { // refill userRegister
            userRegister.put(user.getUid(), users.indexOf(user));
        }
    }

}

