package edu.passau.uni.de.letsgobuddy.domain.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.passau.uni.de.letsgobuddy.R;

/**
 * {@link Fragment} to inflate the {@link View} for the {@link CreateAppointmentActivity}.
 */
public class CreateAppointmentActivityFragment extends Fragment {

    public CreateAppointmentActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_appointment_fragment, container, false);
    }
}
