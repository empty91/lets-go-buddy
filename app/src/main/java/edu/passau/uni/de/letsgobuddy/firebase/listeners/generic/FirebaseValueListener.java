package edu.passau.uni.de.letsgobuddy.firebase.listeners.generic;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import edu.passau.uni.de.letsgobuddy.auth.listeners.SignOutListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * Extension of the {@link ValueEventListener} from the {@link com.google.firebase.database.FirebaseDatabase}.
 * Provides default behavior to minimize Code.
 */
public class FirebaseValueListener implements ValueEventListener {

    private Query queryRef;
    protected DatabaseReference reference;
    protected BaseActivity activity;

    /**
     * {@link java.lang.reflect.Constructor} for a {@link FirebaseValueListener}.
     *
     * @param reference the {@link DatabaseReference}.
     */
    public FirebaseValueListener(DatabaseReference reference) {
        this(reference, false, null);
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link FirebaseValueListener}.
     *
     * @param reference             the {@link DatabaseReference}.
     * @param isSingleEventListener true if {@link DatabaseReference#addListenerForSingleValueEvent(ValueEventListener)}, false otherwise
     */
    public FirebaseValueListener(DatabaseReference reference, boolean isSingleEventListener) {
        this(reference, isSingleEventListener, null);
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link FirebaseValueListener}.
     *
     * @param reference the {@link DatabaseReference}.
     * @param activity  the {@link BaseActivity}
     */
    public FirebaseValueListener(DatabaseReference reference, BaseActivity activity) {
        this(reference, false, activity);
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link FirebaseValueListener}.
     *
     * @param reference the {@link Query}.
     * @param activity  the {@link BaseActivity}
     */
    public FirebaseValueListener(Query reference, BaseActivity activity) {
        this(reference, false, activity);
    }


    /**
     * {@link java.lang.reflect.Constructor} for a {@link FirebaseValueListener}.
     *
     * @param reference             the {@link DatabaseReference}.
     * @param isSingleEventListener true if {@link DatabaseReference#addListenerForSingleValueEvent(ValueEventListener)}, false otherwise.
     * @param activity              the {@link BaseActivity}.
     */
    public FirebaseValueListener(DatabaseReference reference, boolean isSingleEventListener, BaseActivity activity) {
        this.reference = reference;
        this.activity = activity;
        if (isSingleEventListener) {
            reference.addListenerForSingleValueEvent(this);
        } else {
            reference.addValueEventListener(this);
        }
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link FirebaseValueListener}.
     *
     * @param reference             the {@link Query}.
     * @param isSingleEventListener true if {@link DatabaseReference#addListenerForSingleValueEvent(ValueEventListener)}, false otherwise.
     * @param activity              the {@link BaseActivity}
     */
    public FirebaseValueListener(Query reference, boolean isSingleEventListener, BaseActivity activity) {
        this.queryRef = reference;
        this.activity = activity;
        if (isSingleEventListener) {
            reference.addListenerForSingleValueEvent(this);
        } else {
            reference.addValueEventListener(this);
        }
    }

    /**
     * Method to call on {@link SignOutListener#signOut()}.
     */
    public void onSignOut() {
        if (reference != null) {
            reference.removeEventListener(this);
        }
        if (queryRef != null) {
            queryRef.removeEventListener(this);
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
