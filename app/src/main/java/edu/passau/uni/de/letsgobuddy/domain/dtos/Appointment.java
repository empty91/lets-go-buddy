package edu.passau.uni.de.letsgobuddy.domain.dtos;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Dto to represent an {@link Appointment} of the {@link android.app.Application}.
 */
@IgnoreExtraProperties
public class Appointment implements Parcelable, Comparable<Appointment> {

    // unique ID provided by Firebase
    private String uid;

    // name provided by user input
    private String name;

    // date and time provided by user input
    private Long dateAndTime;

    // foreign key of the group it belongs to
    private String groupID;

    // latitude coordinate
    private String latitude;

    // longitude coordinate
    private String longitude;

    // active flag, false if already over
    private boolean active = true;

    // foreign keys of attendants(minified user) which belong to the appointment
    private Map<String, Attendant> members = new HashMap<>();

    // foreign key of the user who created the appointment
    private String createdBy;

    /**
     * Default {@link java.lang.reflect.Constructor}.
     */
    public Appointment() {
    }

    /**
     * {@link java.lang.reflect.Constructor} for an {@link Appointment}.
     *
     * @param uid the {@link Appointment#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     */
    public Appointment(String uid) {
        this.uid = uid;
    }

    /**
     * {@link java.lang.reflect.Constructor} for an {@link Appointment}.
     *
     * @param uid         the {@link Appointment#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     * @param name        the {@link Appointment#name} provided by the {@link User}.
     * @param dateAndTime the {@link Appointment#dateAndTime} provided by the {@link User}.
     * @param groupID     the {@link Appointment#groupID} the {@link Appointment} belongs to.
     * @param members     the {@link Appointment#members} who belong to the {@link Appointment}.
     * @param createdBy   the {@link Appointment#createdBy}.
     */
    public Appointment(String uid, String name, Long dateAndTime, String groupID, Map<String, Attendant> members, String createdBy) {
        this(uid, name, dateAndTime, groupID, null, null, members, createdBy);
    }


    /**
     * {@link java.lang.reflect.Constructor} for an {@link Appointment}.
     *
     * @param uid         the {@link Appointment#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     * @param name        the {@link Appointment#name} provided by the {@link User}.
     * @param dateAndTime the {@link Appointment#dateAndTime} provided by the {@link User}.
     * @param groupID     the {@link Appointment#groupID} the {@link Appointment} belongs to.
     * @param latitude    the latitude coordinate.
     * @param longitude   the longitude coordinate.
     * @param members     the {@link Appointment#members} who belong to the {@link Appointment}.
     * @param createdBy   the {@link Appointment#createdBy}.
     */
    public Appointment(String uid, String name, Long dateAndTime, String groupID, String latitude, String longitude, Map<String, Attendant> members, String createdBy) {
        this.uid = uid;
        this.name = name;
        this.dateAndTime = dateAndTime;
        this.groupID = groupID;
        this.latitude = latitude;
        this.longitude = longitude;
        this.members = members;
        this.createdBy = createdBy;
    }

    /**
     * {@link java.lang.reflect.Constructor} for an {@link Parcelable} {@link Appointment}.
     *
     * @param in the {@link Parcel} to create an {@link Appointment} from.
     */
    public Appointment(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.dateAndTime = in.readLong();
        this.groupID = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.active = in.readByte() != 0;
        in.readMap(members, Map.class.getClassLoader());
        this.createdBy = in.readString();
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(name);
        parcel.writeSerializable(dateAndTime);
        parcel.writeString(groupID);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeByte((byte) (active ? 1 : 0));
        parcel.writeMap(members);
        parcel.writeString(createdBy);
    }

    /**
     * {@link android.os.Parcelable.Creator} for an {@link Appointment}.
     */
    public static final Creator<Appointment> CREATOR = new Creator<Appointment>() {

        /**
         * {@link java.lang.reflect.Constructor} for an {@link Appointment} from a {@link Parcel}.
         * @param in the {@link Parcel}.
         * @return the {@link Appointment}.
         */
        public Appointment createFromParcel(Parcel in) {
            return new Appointment(in);
        }

        /**
         * {@link java.lang.reflect.Constructor} for an {@link Appointment} {@link java.lang.reflect.Array}.
         * @param size the {@link java.lang.reflect.Array} size.
         * @return the {@link Appointment} {@link java.lang.reflect.Array}.
         */
        public Appointment[] newArray(int size) {
            return new Appointment[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Long dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Map<String, Attendant> getMembers() {
        return members;
    }

    public void setMembers(Map<String, Attendant> members) {
        this.members = members;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Appointment that = (Appointment) o;

        return uid.equals(that.uid) && (name != null ? name.equals(that.name) : that.name == null) && (dateAndTime != null ? dateAndTime.equals(that.dateAndTime) : that.dateAndTime == null) && (groupID != null ? groupID.equals(that.groupID) : that.groupID == null) && (latitude != null ? latitude.equals(that.latitude) : that.latitude == null) && (longitude != null ? longitude.equals(that.longitude) : that.longitude == null) && (createdBy != null ? createdBy.equals(that.createdBy) : that.createdBy == null);
    }

    @Override
    public int hashCode() {
        int result = uid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (dateAndTime != null ? dateAndTime.hashCode() : 0);
        result = 31 * result + (groupID != null ? groupID.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        return result;
    }

    public boolean isSet() {
        return this.uid != null && this.name != null && this.dateAndTime != null && this.groupID != null && this.latitude != null && this.longitude != null && this.members != null && this.createdBy != null;
    }


    @Override
    public int compareTo(@NonNull Appointment o) {
        if (this.dateAndTime != null & o.dateAndTime != null) {
            return this.dateAndTime.compareTo(o.dateAndTime);
        } else if (this.dateAndTime != null & o.dateAndTime == null) {
            return 1;
        } else if (o.dateAndTime != null) {
            return -1;
        }
        return 0;
    }
}
