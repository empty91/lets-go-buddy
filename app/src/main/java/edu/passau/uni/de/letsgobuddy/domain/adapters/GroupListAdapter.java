package edu.passau.uni.de.letsgobuddy.domain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;

/**
 * {@link ArrayAdapter} to display the {@link Group groups} of a {@link User}.
 */
public class GroupListAdapter extends ArrayAdapter<Group> {

    private List<Group> groups;

    public GroupListAdapter(@NonNull Context context, @NonNull List<Group> groups) {
        super(context, R.layout.dashboard_group_items_fragment, groups);
        this.groups = groups;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi != null ? vi.inflate(R.layout.dashboard_group_items_fragment, null) : null;
        }

        if (convertView != null) {

            // get Group and set Name
            final Group group = groups.get(position);
            ((TextView) convertView.findViewById(R.id.group_item_name)).setText(group.getName());

            final StringBuilder members = new StringBuilder();
            for (String userID : group.getUsers().values()) {

                // iterate users and get database reference
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(userID);
                final View finalConvertView = convertView;
                usersRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user != null) {

                            if (user.isCurrentUser()) { // inser "you" at start of list
                                members.insert(0, getContext().getString(R.string.you));
                            } else { // insert username after comma
                                members.append(", ").append(user.getName());
                            }
                            ((TextView) finalConvertView.findViewById(R.id.group_item_members)).setText(members.toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(group.getAdmin())) {
                convertView.findViewById(R.id.group_item_admin).setVisibility(View.VISIBLE);
            }

        }
        return convertView;
    }
}

