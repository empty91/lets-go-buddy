package edu.passau.uni.de.letsgobuddy.auth.util;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import edu.passau.uni.de.letsgobuddy.domain.dtos.User;


/**
 * Utility Class to provide some Authentication related Methods.
 */
public class AuthenticationUtil {

    /**
     * Checks if the provided {@link User#getUid() User ID} matched the current {@link User}.
     *
     * @param userID the {@link User#getUid()}
     * @return true if current, false otherwise
     */
    public static boolean isCurrentUser(String userID) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser != null && currentUser.getUid().equals(userID);
    }
}
