package edu.passau.uni.de.letsgobuddy.firebase.listeners.singleEventListeners;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import edu.passau.uni.de.letsgobuddy.domain.activities.AppointmentInfoActivity;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


/**
 * {@link FirebaseValueListener} to single load a {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Group}.
 */
public class GroupLoadSingleListener extends FirebaseValueListener {


    /**
     * {@link java.lang.reflect.Constructor} for a {@link GroupLoadSingleListener}.
     *
     * @param reference the {@link DatabaseReference}.
     */
    public GroupLoadSingleListener(DatabaseReference reference, BaseActivity activity) {
        super(reference, true, activity);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        Group group = dataSnapshot.getValue(Group.class);
        if (group != null && activity instanceof AppointmentInfoActivity) {
            ((AppointmentInfoActivity) activity).setGroup(group);
        }
    }
}