package edu.passau.uni.de.letsgobuddy.generic.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.auth.activities.LoginActivity;
import edu.passau.uni.de.letsgobuddy.auth.listeners.SignOutListener;
import edu.passau.uni.de.letsgobuddy.domain.activities.DashboardActivity;
import edu.passau.uni.de.letsgobuddy.domain.util.MenuInflaterUtil;

/**
 * Base {@link Activity} all other {@link android.app.Activity activities} have to extend.
 * Provides the {@link Toolbar} and {@link ProgressBar}, as well as the {@link SignOutListener#signOut() Signout Method}
 */
public class BaseActivity extends AppCompatActivity {

    /**
     * The {@link ProgressBar} to show loading animation.
     */
    protected ProgressBar progressBar;

    /**
     * The {@link Toolbar} to provide an options {@link Menu}.
     */
    protected Toolbar toolbar;

    /**
     * The {@link SignOutListener} to provide the {@link SignOutListener#signOut() Signout Method}.
     */
    protected SignOutListener signOutListener;

    /**
     * Flag to indicate Admin privileges.
     */
    protected boolean isAdmin;

    /**
     * The {@link Menu}.
     */
    protected Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        signOutListener = new SignOutListener(this);
        setContentView(R.layout.base_activity);
        prepareNewLayout();
    }

    /**
     * Sets the correct {@link Layout} for the {@link BaseActivity}.
     * You always have to call this method to override the default {@link Layout}.
     *
     * @param layoutResID resource id of the {@link Layout}
     */
    protected void layoutOnCreate(int layoutResID) {

        setContentView(layoutResID);
        prepareNewLayout();
        showLoadingScreen();
    }

    /**
     * Prepares the {@link ProgressBar} and {@link Toolbar}.
     */
    private void prepareNewLayout() {

        progressBar = findViewById(R.id.progressbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            if (!(this instanceof DashboardActivity | this instanceof LoginActivity)) {
                supportActionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

    }

    /**
     * Toggles the {@link ProgressBar}.
     */
    public void toggleLoadingScreen() {

        if (progressBar.getVisibility() == View.GONE) {
            showLoadingScreen();
        } else {
            hideLoadingScreen();
        }
    }

    /**
     * Shows the {@link ProgressBar}.
     */
    public void showLoadingScreen() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hides the {@link ProgressBar}.
     */
    public void hideLoadingScreen() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        if (!isAdmin) {
            MenuInflaterUtil.inflateMenu(this, menu);
        } else {
            MenuInflaterUtil.inflateAdmin(this, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_logout:
                signOutListener.signOut();
                return true;

            case android.R.id.home:
                navigateBack();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        navigateBack();
    }

    /**
     * Navigates back to the parent {@link Activity}.
     */
    private void navigateBack() {
        Intent intent = NavUtils.getParentActivityIntent(this);
        if (intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            NavUtils.navigateUpTo(this, intent);
        }
    }

    /**
     * Method which should be invoked on {@link SignOutListener#signOut()}.
     */
    public void onSignOut() {

    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Menu getMenu() {
        return menu;
    }
}
