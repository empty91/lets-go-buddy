package edu.passau.uni.de.letsgobuddy.domain.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;

/**
 * {@link DialogFragment} to create new {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Group}.
 */
public class CreateGroupDialog extends DialogFragment {


    @SuppressLint("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.group_create);
        final View dialogView = getActivity().getLayoutInflater().inflate(R.layout.create_group_fragment, null);
        builder.setView(dialogView);
        dialogView.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateGroupDialog.this.dismiss();
            }
        });

        dialogView.findViewById(R.id.create_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText groupName = dialogView.findViewById(R.id.group_name);
                if (groupName != null) {
                    if (!TextUtils.isEmpty(groupName.getText())) {
                        try {
                            // persist new group
                            FirebaseDao.persistGroup(groupName.getText().toString());
                        } catch (Exception e) {
                            Log.d("CRITCAL", "onClick: No currently signed in User");
                        }
                        CreateGroupDialog.this.dismiss();
                    } else {
                        groupName.setError(getText(R.string.group_name_empty));
                    }
                }
            }
        });
        return builder.create();
    }


}