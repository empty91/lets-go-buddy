package edu.passau.uni.de.letsgobuddy.domain.activities;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.util.EmailValidationUtil;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * This Activity is used to add a {@link edu.passau.uni.de.letsgobuddy.domain.dtos.User} to a {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Group}
 */
public class AddUserActivity extends BaseActivity {

    private EditText enterEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.adduser_activity);
        prepareUI();

    }

    private void prepareUI() {

        this.hideLoadingScreen();

        Bundle extras = getIntent().getExtras();
        String groupId = null;
        if (extras != null) {
            groupId = extras.getString(Constants.GROUP_ID);
        }

        enterEmail = findViewById(R.id.email_input);
        Button addButton = findViewById(R.id.add_user_button);

        final String finalGroupId = groupId;
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userEmail = enterEmail.getText().toString();
                userEmail = userEmail.toLowerCase();
                if (TextUtils.isEmpty(userEmail)) {
                    enterEmail.setError(getResources().getString(R.string.empty));
                } else if (!EmailValidationUtil.isEmailValid(userEmail)) {
                    enterEmail.setError(getResources().getString(R.string.invalidemail));
                } else {
                    FirebaseDao.addUser(userEmail, finalGroupId, AddUserActivity.this);
                }
            }

        });
    }

    /**
     * Shows success {@link Toast}.
     */
    public void success() {
        Toast toast = Toast.makeText(this, getResources().getString(R.string.success), Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * Sets an error on {@link EditText#setError(CharSequence) enterEmail}.
     */
    public void failed() {
        enterEmail.setError(getResources().getString(R.string.add_user_fail));
    }

    /**
     * Sets an error on {@link EditText#setError(CharSequence) enterEmail}.
     */
    public void alreadyInGroup() {
        enterEmail.setError(getResources().getString(R.string.user_already_in_group));
    }

    /**
     * Sets the {@link EditText#getText()} to an empty {@link String}.
     */
    public void setEditTextBlank() {
        enterEmail.setText("");
    }
}
