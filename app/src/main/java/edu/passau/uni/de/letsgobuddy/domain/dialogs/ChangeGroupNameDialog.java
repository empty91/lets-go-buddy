package edu.passau.uni.de.letsgobuddy.domain.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;

/**
 * {@link DialogFragment} to change the {@link Group#name}.
 */
public class ChangeGroupNameDialog extends DialogFragment {

    @SuppressLint("InflateParams")
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.group_change_name);
        final View dialogView = getActivity().getLayoutInflater().inflate(R.layout.change_group_name_fragment, null);
        builder.setView(dialogView);
        dialogView.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeGroupNameDialog.this.dismiss();
            }
        });
        final Bundle arguments = this.getArguments();

        dialogView.findViewById(R.id.change_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText groupName = dialogView.findViewById(R.id.group_name);
                if (groupName != null) {
                    if (!TextUtils.isEmpty(groupName.getText())) {
                        try {
                            Group group = arguments.getParcelable(Constants.GROUP);
                            if (group != null) {
                                group.setName(String.valueOf(groupName.getText()));
                                Group changedGroup = FirebaseDao.changeGroupName(group);
                            }
                        } catch (Exception e) {
                            Log.d("CRITCAL", "onClick: No currently signed in User");
                        }
                        ChangeGroupNameDialog.this.dismiss();
                    } else {
                        groupName.setError(getText(R.string.group_name_empty));
                    }
                }
            }
        });
        return builder.create();
    }
}