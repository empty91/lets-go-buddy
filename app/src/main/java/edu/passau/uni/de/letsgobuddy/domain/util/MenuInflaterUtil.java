package edu.passau.uni.de.letsgobuddy.domain.util;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.auth.activities.LoginActivity;
import edu.passau.uni.de.letsgobuddy.domain.activities.AppointmentInfoActivity;
import edu.passau.uni.de.letsgobuddy.domain.activities.DashboardActivity;
import edu.passau.uni.de.letsgobuddy.domain.activities.GroupViewActivity;

/**
 * Utility class to inflate the {@link Menu} of a {@link android.text.Layout}.
 */
public class MenuInflaterUtil {

    /**
     * Inflates the non-admin {@link android.text.Layout}.
     *
     * @param activity the current {@link android.app.Activity}.
     * @param menu     the {@link Menu} to inflate.
     */
    public static void inflateMenu(AppCompatActivity activity, Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = activity.getMenuInflater();
        if (activity instanceof LoginActivity) {
            menuInflater.inflate(R.menu.menu_login, menu);
        } else if (activity instanceof DashboardActivity) {
            menuInflater.inflate(R.menu.menu_dashboard, menu);
        } else if (activity instanceof GroupViewActivity) {
            menuInflater.inflate(R.menu.menu_groupview, menu);
        } else if (activity instanceof AppointmentInfoActivity) {
            menuInflater.inflate(R.menu.menu_appointmentinfo, menu);
        }
    }

    /**
     * Inflates the admin {@link android.text.Layout}.
     *
     * @param activity the current {@link android.app.Activity}.
     * @param menu     the {@link Menu} to inflate.
     */
    public static void inflateAdmin(AppCompatActivity activity, Menu menu) {
        MenuInflater menuInflater = activity.getMenuInflater();
        if (activity instanceof GroupViewActivity) {
            menuInflater.inflate(R.menu.menu_groupview_admin, menu);
        } else if (activity instanceof AppointmentInfoActivity) {
            menuInflater.inflate(R.menu.menu_appointmentinfo_admin, menu);
        }
    }
}

