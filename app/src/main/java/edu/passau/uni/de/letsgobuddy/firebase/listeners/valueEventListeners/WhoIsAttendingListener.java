package edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.passau.uni.de.letsgobuddy.domain.activities.WhoIsAttendingActivity;
import edu.passau.uni.de.letsgobuddy.domain.adapters.AttendingListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * {@link FirebaseValueListener} to listen to the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Attendant attendants} of an {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment}.
 */
public class WhoIsAttendingListener extends FirebaseValueListener {

    private Group group;
    private List<User> users;
    private Map<String, Integer> userRegister = new HashMap<>();
    private AttendingListAdapter attendingListAdapter;

    /**
     * {@link java.lang.reflect.Constructor} for a {@link WhoIsAttendingListener}.
     *
     * @param group                the {@link Group} .
     * @param users                the {@link User}.
     * @param attendingListAdapter the {@link AttendingListAdapter} to notify.
     * @param activity             the {@link BaseActivity} the {@link WhoIsAttendingListener} is registered to.
     * @param reference            the {@link DatabaseReference}.
     */
    public WhoIsAttendingListener(Group group, List<User> users, AttendingListAdapter attendingListAdapter, BaseActivity activity, DatabaseReference reference) {
        super(reference, activity);
        this.group = group;
        this.users = users;
        this.attendingListAdapter = attendingListAdapter;
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        activity.showLoadingScreen();
        group = dataSnapshot.getValue(Group.class);
        if (group != null) {
            if (activity instanceof WhoIsAttendingActivity) {
                attendingListAdapter.setGroup(group);

                // set admin privileges
                activity.setAdmin(group.isAdmin(FirebaseAuth.getInstance().getCurrentUser().getUid()));
                ((WhoIsAttendingActivity) activity).setGroup(group);

                // iterate over all users
                for (String userUid : group.getUsers().values()) {
                    Integer index = userRegister.get(userUid);
                    User user = new User(userUid);
                    if (index != null && index < group.getUsers().values().size()) { // user exists and is not out of bounds
                        users.remove(index.intValue()); //reinsert
                        users.add(index, user);
                    } else { // user does not exist, insert
                        users.add(user);
                        userRegister.put(userUid, users.indexOf(user));
                    }
                }

                cleanRegister();
                attendingListAdapter.notifyDataSetChanged();
            }
        }
        activity.hideLoadingScreen();
    }

    /**
     * Cleans the userregister to make sure there are no duplicate entries of wrong indizes.
     */
    private void cleanRegister() {

        // keys to remove from the userRegister
        List<String> userRegisterRemove = new LinkedList<>();

        // collect all keys to remove
        for (String userRegisterKey : userRegister.keySet()) {
            if (!group.getUsers().values().contains(userRegisterKey)) {
                userRegisterRemove.add(userRegisterKey);
            }
        }

        List<User> usersRemove = new LinkedList<>();

        for (String userRegisterRemoveKey : userRegisterRemove) {

            // remove from register
            userRegister.remove(userRegisterRemoveKey);
            for (User user : users) { // collect users to remove
                if (user.getUid().equals(userRegisterRemoveKey)) {
                    usersRemove.add(user);
                }
            }
        }

        userRegister.clear();
        users.removeAll(usersRemove);
        for (User user : users) { // refill userRegister
            userRegister.put(user.getUid(), users.indexOf(user));
        }
    }

}

