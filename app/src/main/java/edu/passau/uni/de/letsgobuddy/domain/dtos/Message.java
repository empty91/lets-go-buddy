package edu.passau.uni.de.letsgobuddy.domain.dtos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Dto to represent a {@link Message} of the {@link android.app.Application}.
 * This is used to send {@link android.app.Notification notifcations} through {@link com.google.firebase.messaging.FirebaseMessaging}.
 */
@IgnoreExtraProperties
public class Message implements Parcelable {

    // unique ID provided by Firebase
    private String uid;

    // foreign key of the appointment it belongs to
    private String appointmentID;

    // german message
    private String textDe;

    // english message
    private String textEn;

    /**
     * Default {@link java.lang.reflect.Constructor}.
     */
    public Message() {
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link Message}.
     *
     * @param uid the {@link Message#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     */
    public Message(String uid) {
        this.uid = uid;
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link Message}.
     *
     * @param uid           the {@link Message#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     * @param appointmentID the {@link Message#appointmentID} the {@link Message} belongs to.
     * @param textDe        the {@link java.util.Locale#GERMAN} message.
     * @param textEn        the {@link java.util.Locale#ENGLISH} message.
     */
    public Message(String uid, String appointmentID, String textDe, String textEn) {
        this.uid = uid;
        this.appointmentID = appointmentID;
        this.textDe = textDe;
        this.textEn = textEn;
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link Parcelable} {@link Message}.
     *
     * @param in the {@link Parcel} to create a {@link Message} from.
     */
    public Message(Parcel in) {
        this.uid = in.readString();
        this.appointmentID = in.readString();
        this.textDe = in.readString();
        this.textEn = in.readString();
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(appointmentID);
        parcel.writeString(textDe);
        parcel.writeString(textEn);
    }

    /**
     * {@link android.os.Parcelable.Creator} for a {@link Message}.
     */
    public static final Creator<Message> CREATOR = new Creator<Message>() {

        /**
         * {@link java.lang.reflect.Constructor} for a {@link Message} from a {@link Parcel}.
         * @param in the {@link Parcel}.
         * @return the {@link Message}.
         */
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        /**
         * {@link java.lang.reflect.Constructor} for a {@link Message} {@link java.lang.reflect.Array}.
         * @param size the {@link java.lang.reflect.Array} size.
         * @return the {@link Message} {@link java.lang.reflect.Array}.
         */
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return uid.equals(message.uid) && (appointmentID != null ? appointmentID.equals(message.appointmentID) : message.appointmentID == null) && (textDe != null ? textDe.equals(message.textDe) : message.textDe == null) && (textEn != null ? textEn.equals(message.textEn) : message.textEn == null);
    }

    @Override
    public int hashCode() {
        int result = uid.hashCode();
        result = 31 * result + (appointmentID != null ? appointmentID.hashCode() : 0);
        result = 31 * result + (textDe != null ? textDe.hashCode() : 0);
        result = 31 * result + (textEn != null ? textEn.hashCode() : 0);
        return result;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getTextDe() {
        return textDe;
    }

    public void setTextDe(String textDe) {
        this.textDe = textDe;
    }

    public String getTextEn() {
        return textEn;
    }

    public void setTextEn(String textEn) {
        this.textEn = textEn;
    }
}
