package edu.passau.uni.de.letsgobuddy;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;

/**
 * {@link FirebaseInstanceIdService} to store {@link com.google.firebase.messaging.FirebaseMessaging} tokens.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIdService";

    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // save token to locale preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.edit().putString(Constants.FIREBASE_TOKEN, refreshedToken).apply();

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) { // if user logged in send to server
            saveToken(getApplicationContext(), currentUser.getUid());
        }
    }

    /**
     * Saves provided token to the {@link FirebaseDatabase}.
     *
     * @param context the {@link Context}
     * @param uid     the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.User#uid}
     */
    public static void saveToken(Context context, String uid) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String token = preferences.getString(Constants.FIREBASE_TOKEN, null);

        if (token != null) { // store token
            DatabaseReference tokensRef = FirebaseDatabase.getInstance().getReference().child(Constants.TOKENS).child(uid).child(Constants.TOKEN);
            tokensRef.setValue(token);
        }
    }
}
