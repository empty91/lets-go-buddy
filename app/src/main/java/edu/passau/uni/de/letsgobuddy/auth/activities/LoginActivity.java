package edu.passau.uni.de.letsgobuddy.auth.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import edu.passau.uni.de.letsgobuddy.MyFirebaseInstanceIdService;
import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.auth.listeners.SignInListener;
import edu.passau.uni.de.letsgobuddy.domain.activities.DashboardActivity;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


/**
 * {@link Activity}ty to handle {@link FirebaseAuth#signInWithCredential(AuthCredential)}.
 */
public class LoginActivity extends BaseActivity {

    /**
     * The {@link GoogleSignInClient} for authorization with Google.
     */
    private GoogleSignInClient googleSignInClient;

    private SignInButton signInButton;

    /**
     * The introduction {@link android.media.Image}
     */
    private ImageView introImage;

    /**
     * The {@link FirebaseAuth}
     */
    private FirebaseAuth firebaseAuth;

    /**
     * The {@link SignInListener} to handle the login.
     */
    private SignInListener signInListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        prepareUI();
        prepareAuth();
        hideLoadingScreen();
    }

    /**
     * Perpares the {@link android.text.Layout}.
     */
    private void prepareUI() {

        super.layoutOnCreate(R.layout.login_activity);
        introImage = (ImageView) findViewById(R.id.intro_image);
    }

    /**
     * Prepares the {@link LoginActivity#googleSignInClient} and {@link LoginActivity#firebaseAuth}.
     */
    @SuppressLint("RestrictedApi")
    private void prepareAuth() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        firebaseAuth = FirebaseAuth.getInstance();
        prepareSignInButton();
    }

    /**
     * Prepares the {@link android.widget.Button} to login with.
     */
    private void prepareSignInButton() {

        signInListener = new SignInListener(this);
        findViewById(R.id.sign_in_button).setOnClickListener(signInListener);
    }

    @Override
    public void onStart() {

        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) { // if already logged in
            goToDashboard(currentUser.getUid());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        signInListener.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void toggleLoadingScreen() {

        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoadingScreen() {
        super.showLoadingScreen();
        findViewById(R.id.sign_in_button).setVisibility(View.GONE);
    }

    @Override
    public void hideLoadingScreen() {
        super.hideLoadingScreen();
        findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
    }

    /**
     * Shows the introImage.
     */
    public void showImage() {
        introImage.setVisibility(View.VISIBLE);
    }

    /**
     * Hides the introImage.
     */
    public void hideImage() {
        introImage.setVisibility(View.INVISIBLE);
    }


    /**
     * Navigate to {@link DashboardActivity}.
     *
     * @param uid the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.User}'s uid.
     */

    public void goToDashboard(String uid) {

        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra(Constants.USER_ID, uid);
        startActivity(intent);
        MyFirebaseInstanceIdService.saveToken(getApplicationContext(), uid);
    }

    public GoogleSignInClient getGoogleSignInClient() {
        return googleSignInClient;
    }

    public FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }
}
