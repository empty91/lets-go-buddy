package edu.passau.uni.de.letsgobuddy.domain.dtos;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Dto to represent a {@link User} of the {@link android.app.Application}.
 * Gets its information from the {@link FirebaseAuth#getCurrentUser()}.
 */
@IgnoreExtraProperties
public class User implements Parcelable, Comparable<User> {

    // unique ID provided by Firebase
    private String uid;

    // name provided by google account
    private String name;

    // email provided by google
    private String email;

    // foreign keys of groups the user belongs to
    private Map<String, String> groups = new HashMap<>();

    /**
     * Default {@link java.lang.reflect.Constructor}.
     */
    public User() {
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link User}.
     *
     * @param uid the {@link User#uid} provided by {@link FirebaseAuth}.
     */
    public User(String uid) {
        this.uid = uid;
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link User}.
     *
     * @param uid   the {@link User#uid} provided by {@link FirebaseAuth}.
     * @param name  the {@link User#name} provided by Google.
     * @param email the {@link User#email} provided by Google.
     */
    public User(String uid, String name, String email) {
        this(uid, name, email, new HashMap<String, String>());
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link User}.
     *
     * @param uid    the {@link User#uid} provided by {@link FirebaseAuth}.
     * @param name   the {@link User#name} provided by Google.
     * @param email  the {@link User#email} provided by Google.
     * @param groups the {@link User#groups} keys the {@link User} belongs to.
     */
    public User(String uid, String name, String email, Map<String, String> groups) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.groups = groups;
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link Parcelable} {@link User}.
     *
     * @param in the {@link Parcel} to create a {@link User} from.
     */
    public User(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        in.readMap(groups, Map.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeMap(groups);
    }

    /**
     * {@link android.os.Parcelable.Creator} for a {@link User}.
     */
    public static final Creator<User> CREATOR = new Creator<User>() {

        /**
         * {@link java.lang.reflect.Constructor} for a {@link User} from a {@link Parcel}.
         * @param in the {@link Parcel}.
         * @return the {@link User}.
         */
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        /**
         * {@link java.lang.reflect.Constructor} for a {@link User} {@link java.lang.reflect.Array}.
         * @param size the {@link java.lang.reflect.Array} size.
         * @return the {@link User} {@link java.lang.reflect.Array}.
         */
        public User[] newArray(int size) {
            return new User[size];
        }
    };


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> getGroups() {
        return groups;
    }

    public void setGroups(Map<String, String> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return uid.equals(user.uid) && (name != null ? name.equals(user.name) : user.name == null) && (email != null ? email.equals(user.email) : user.email == null);
    }

    @Override
    public int hashCode() {
        int result = uid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(@NonNull User o) {
        return this.name.compareTo(o.getName());
    }

    /**
     * Checks whether the currently logged in {{@link FirebaseAuth#getCurrentUser()}} is this {@link User}.
     *
     * @return true if currently logged in {@link User}, false otherwise.
     */
    @Exclude
    public boolean isCurrentUser() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser != null && currentUser.getUid().equals(this.getUid());
    }

    /**
     * Checks whether this {@link User} is the {@link Group#admin} of a {@link Group}.
     *
     * @param group the {@link Group}.
     * @return true if admin, false otherwise.
     */
    @Exclude
    public boolean isGroupAdmin(Group group) {
        return group.getAdmin() != null && group.getAdmin().equals(this.getUid());
    }
}
