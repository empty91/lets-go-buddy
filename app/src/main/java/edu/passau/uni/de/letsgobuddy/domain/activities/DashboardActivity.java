package edu.passau.uni.de.letsgobuddy.domain.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.adapters.GroupListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.listeners.ShowCreateGroupListener;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.DashboardGroupListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


/**
 * {@link Activity} to display the Dashboard with the {@link Group} List.
 */
public class DashboardActivity extends BaseActivity {

    private Map<String, Integer> groupRegister = new HashMap<>();
    private List<Group> groups = new LinkedList<>();
    private GroupListAdapter groupListAdapter;
    private DashboardGroupListener dashboardGroupListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.dashboard_activity);
        prepareUI();
        prepareContent();
    }

    /**
     * Prepares the {@link android.text.Layout}
     */
    private void prepareUI() {

        // init list and listViewAdapter
        groupListAdapter = new GroupListAdapter(DashboardActivity.this, groups);
        ListView listView = findViewById(R.id.dashboard_list);
        if (listView != null) {
            listView.setAdapter(groupListAdapter);
        }

        FloatingActionButton fab = findViewById(R.id.dashboard_create_group);
        fab.setOnClickListener(new ShowCreateGroupListener(this));

    }

    /**
     * Prepares the Content by loading data from the {@link FirebaseDatabase}.
     */
    private void prepareContent() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String userID = String.valueOf(extras.get(Constants.USER_ID));
            if (userID != null) {

                // get User reference
                DatabaseReference groupsNode = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(userID).child(Constants.GROUPS);
                dashboardGroupListener = new DashboardGroupListener(groups, groupRegister, groupListAdapter, this, groupsNode);


                // set listener to navigate to GroupViewActivity on group click
                ListView list = findViewById(R.id.dashboard_list);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Group group = groups.get(position);
                        if (group != null) {
                            Intent intent = new Intent(DashboardActivity.this, GroupViewActivity.class);
                            intent.putExtra(Constants.GROUP_ID, group.getUid());
                            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                            startActivity(intent);
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        restart();
        prepareUI();
        prepareContent();
    }

    private void restart() {
        groups.clear();
        groupRegister.clear();
    }

    @Override
    public void onSignOut() {
        dashboardGroupListener.onSignOut();
    }

}

