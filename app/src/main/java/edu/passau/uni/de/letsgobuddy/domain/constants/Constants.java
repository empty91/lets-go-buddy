package edu.passau.uni.de.letsgobuddy.domain.constants;

/**
 * Constants to be used to either transfer Data between {@link android.app.Activity activities} or to
 * fetch Data from the {@link com.google.firebase.database.FirebaseDatabase}.
 */
public final class Constants {
    public static final String USERS = "users";
    public static final String GROUP = "group";
    public static final String GROUPS = "groups";
    public static final String APPOINTMENTS = "appointments";
    public static final String MESSAGES = "messages";
    public static final String USER_ID = "userID";
    public static final String GROUP_ID = "groupID";
    public static final String APPOINTMENT_ID = "appointmentID";
    public static final String NAME = "name";
    public static final String ADMIN = "admin";
    public static final String EMAIL = "email";
    public static final String SIGN_IN_SUCCESS = "Login successful";
    public static final String SIGN_IN_ERROR = "Login failed";
    public static final String FIREBASE_ERROR = "Firebase Error occurred";
    public static final String USER_NULL = "Firebase user is null";
    public static final String SILENT_SIGN_IN_ERROR = "Silent signin failed. No access granted.";
    public static final String CREATE_GROUP = "Create new Group";
    public static final String CHANGE_GROUP_NAME = "Change Group name";
    public static final String MAKE_ADMIN_DIALOG = "make user admin";
    public static final String KICK_MEMBER_DIALOG = "kick member";
    public static final String LEAVE_GROUP_DIALOG = "leave group";
    public static final String DELETE_GROUP_DIALOG = "delete group";
    public static final String FIREBASE_TOKEN = "firebase token";
    public static final String MEMBERS = "members";
    public static final String APPOINTMENT = "appointment";
    public static final String ATTENDING = "attending";
    public static final String CANCEL_APPOINTMENT = "cancel appointment";
    public static final String TITLE = "title";
    public static final String TEXT_DE = "textDe";
    public static final String TEXT_EN = "textEn";
    public static final String TOKENS = "tokens";
    public static final String TOKEN = "token";
    public static final String EXPIRED = "expired";

    /**
     * Method to get the correct {@link Constants} by key.
     *
     * @param key the key of the {@link Constants}.
     * @return the {@link Constants value} or null if not found.
     */
    public static String getConstant(String key) {
        try {
            return (String) Constants.class.getField(key).get(Constants.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
