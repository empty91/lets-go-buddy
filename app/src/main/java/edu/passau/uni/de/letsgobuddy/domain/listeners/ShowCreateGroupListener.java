package edu.passau.uni.de.letsgobuddy.domain.listeners;

import android.app.DialogFragment;
import android.view.View;

import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.CreateGroupDialog;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


public class ShowCreateGroupListener implements View.OnClickListener {

    private BaseActivity activity;

    public ShowCreateGroupListener(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        DialogFragment dialogFragment = new CreateGroupDialog();
        dialogFragment.show(activity.getFragmentManager(), Constants.CREATE_GROUP);
    }
}
