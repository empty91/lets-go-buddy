package edu.passau.uni.de.letsgobuddy.domain.activities;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.auth.util.AuthenticationUtil;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Attendant;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.util.DateFormatUtil;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.singleEventListeners.CreateAppointmentGroupLoadListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * {@link Activity} to create an {@link Appointment}.
 */
public class CreateAppointmentActivity extends BaseActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private final int PLACE_PICKER_REQUEST = 1;
    private Group group = new Group();
    private Calendar calendar = new GregorianCalendar();
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private Place place;
    private boolean isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.create_appointment_activity);
        prepareUI();
        prepareContent();
        prepareListeners();
        prepareTimeAndDatePicker();
    }

    /**
     * Callback for choosing a {@link Place} using the {@link PlacePicker}.
     *
     * @param requestCode the requestCode
     * @param resultCode  the resultCode
     * @param data        the returned data
     */
    @SuppressLint("RestrictedApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                place = PlacePicker.getPlace(this, data);
                EditText location = findViewById(R.id.create_appointment_location);
                location.setText(place.getAddress());
            } else {
                Toast toast = Toast.makeText(this, getResources().getString(R.string.place_picking_failed), Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    /**
     * Prepares the {@link android.text.Layout}
     */
    private void prepareUI() {

    }

    /**
     * Prepares the Listeners for the {@link View}
     */
    private void prepareListeners() {

        EditText date = findViewById(R.id.create_appointment_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove possible errors onClick
                ((EditText) findViewById(R.id.create_appointment_date)).setError(null);
                datePickerDialog.show();
            }
        });
        EditText time = findViewById(R.id.create_appointment_time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove possible errors onClick
                ((EditText) findViewById(R.id.create_appointment_time)).setError(null);
                timePickerDialog.show();
            }
        });

        EditText location = findViewById(R.id.create_appointment_location);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove possible errors onClick
                ((EditText) findViewById(R.id.create_appointment_location)).setError(null);
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(CreateAppointmentActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        findViewById(R.id.create_appointment_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.create_appointment_button:
                        create();
                        break;
                }
            }
        });
    }

    /**
     * Prepares the Content by loading data from the {@link FirebaseDatabase}.
     */
    private void prepareContent() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String groupID = String.valueOf(extras.get(Constants.GROUP_ID));
            isAdmin = extras.getBoolean(Constants.ADMIN);
            if (groupID != null) {
                DatabaseReference groupIDNode = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID);
                new CreateAppointmentGroupLoadListener(this, groupIDNode);
            }
        }
    }

    /**
     * Prepares the {@link DatePickerDialog} and {@link TimePickerDialog}.
     */
    private void prepareTimeAndDatePicker() {

        datePickerDialog = new DatePickerDialog(this, CreateAppointmentActivity.this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        timePickerDialog = new TimePickerDialog(this, CreateAppointmentActivity.this,
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
    }

    /**
     * Checks the TimePicker for errors and displays them
     *
     * @param view
     * @param hourOfDay
     * @param minute
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        EditText time = findViewById(R.id.create_appointment_time);
        Calendar currentDate = Calendar.getInstance();

        if (calendar.get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH)
                && calendar.get(Calendar.MONTH) == currentDate.get(Calendar.MONTH)
                && calendar.get(Calendar.YEAR) == currentDate.get(Calendar.YEAR)
                && ((calendar.get(Calendar.HOUR_OF_DAY) < currentDate.get(Calendar.HOUR_OF_DAY)
                || ((calendar.get(Calendar.HOUR_OF_DAY) == currentDate.get(Calendar.HOUR_OF_DAY)
                && (calendar.get(Calendar.MINUTE) < currentDate.get(Calendar.MINUTE))))))) {
            time.setError(getResources().getString(R.string.appointment_time_in_the_past));
            Toast pastTimeToast = Toast.makeText(this, getResources().getString(R.string.appointment_time_in_the_past), Toast.LENGTH_SHORT);
            pastTimeToast.show();
        } else {
            time.setError(null);
            time.setText(DateFormatUtil.formatTime(calendar));
        }
        prepareTimeAndDatePicker();
    }

    /**
     * Checks the DatePicker for errors and displays them
     *
     * @param view
     * @param year
     * @param month
     * @param dayOfMonth
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        EditText date = findViewById(R.id.create_appointment_date);
        Calendar currentDate = Calendar.getInstance();

        if (calendar.get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH)
                && calendar.get(Calendar.MONTH) == currentDate.get(Calendar.MONTH)
                && calendar.get(Calendar.YEAR) == currentDate.get(Calendar.YEAR)) {
            date.setError(null);
            date.setText(DateFormatUtil.formatDate(this, calendar));

        } else if (calendar.getTime().before(currentDate.getTime())) {
            date.setText(null);
            date.setError(getResources().getString(R.string.appointment_date_in_the_past));
            Toast pastDateToast = Toast.makeText(this, getResources().getString(R.string.appointment_date_in_the_past), Toast.LENGTH_SHORT);
            pastDateToast.show();
        } else {
            date.setError(null);
            date.setText(DateFormatUtil.formatDate(this, calendar));
        }
        prepareTimeAndDatePicker();
    }

    /**
     * Creates a new {@link Appointment}.
     */
    public void create() {

        // set members map
        Map<String, Attendant> members = new HashMap<>();
        for (String userUid : group.getUsers().keySet()) {
            members.put(userUid, new Attendant(userUid, AuthenticationUtil.isCurrentUser(userUid)));
        }


        /*
        All EditTexts are assigned to a String variable
        Test if they are empty
        If nothing is empty -> Create Appointment
        */
        boolean empty = false;

        String appointmentName = ((EditText) findViewById(R.id.create_appointment_name)).getText().toString();
        String appointmentDate = ((EditText) findViewById(R.id.create_appointment_date)).getText().toString();
        String appointmentTime = ((EditText) findViewById(R.id.create_appointment_time)).getText().toString();
        String appointmentLocation = ((EditText) findViewById(R.id.create_appointment_location)).getText().toString();


        if (TextUtils.isEmpty(appointmentName)) {
            ((EditText) findViewById(R.id.create_appointment_name)).setError(getResources().getString(R.string.appointment_name_empty));
            empty = true;
        }


        if (TextUtils.isEmpty(appointmentDate)) {
            ((EditText) findViewById(R.id.create_appointment_date)).setError(getResources().getString(R.string.appointment_date_empty));
            (findViewById(R.id.create_appointment_date)).setFocusable(true);
            empty = true;
        }

        if (TextUtils.isEmpty(appointmentTime)) {
            ((EditText) findViewById(R.id.create_appointment_time)).setError(getResources().getString(R.string.appointment_time_empty));
            (findViewById(R.id.create_appointment_time)).setFocusable(true);
            empty = true;
        }

        if (TextUtils.isEmpty(appointmentLocation)) {
            ((EditText) findViewById(R.id.create_appointment_location)).setError(getResources().getString(R.string.appointment_location_empty));
            (findViewById(R.id.create_appointment_location)).setFocusable(true);
            empty = true;
        }

        if (!empty) {
            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
            Appointment appointment = new Appointment(null, appointmentName,
                    calendar.getTimeInMillis(), group.getUid(), place != null ? String.valueOf(place.getLatLng().latitude) : null,
                    place != null ? String.valueOf(place.getLatLng().longitude) : null, members, currentUser.getUid());
            FirebaseDao.persistAppointment(group, appointment, this);
            goToGroupView();
        }
    }


    /**
     * Navigate back to the {@link GroupViewActivity}
     */
    private void goToGroupView() {
        Intent intent = new Intent(CreateAppointmentActivity.this, GroupViewActivity.class);
        intent.putExtra(Constants.GROUP_ID, group.getUid());
        intent.putExtra(Constants.ADMIN, isAdmin);
        startActivity(intent);
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
