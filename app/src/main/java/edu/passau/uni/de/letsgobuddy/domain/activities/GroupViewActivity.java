package edu.passau.uni.de.letsgobuddy.domain.activities;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.LinkedList;
import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.adapters.AppointmentListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.adapters.UserListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.ChangeGroupNameDialog;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.DeleteGroupDialog;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.LeaveGroupDialog;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.AppointmentLoadListener;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.GroupLoadListener;

public class GroupViewActivity extends edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity implements LeaveGroupDialog.NoticeDialogListener, DeleteGroupDialog.NoticeDialogListener {

    private Group group = new Group();
    private UserListAdapter userListAdapter;
    private List<User> users = new LinkedList<>();
    private AppointmentListAdapter appointmentListAdapter;
    private List<Appointment> appointments = new LinkedList<>();
    private String groupID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.groupview_activity);

        prepareUI();
        prepareContent();
    }

    /**
     * Prepares the {@link android.text.Layout}
     */
    private void prepareUI() {

        // init list and listViewAdapter
        userListAdapter = new UserListAdapter(GroupViewActivity.this, group, users);
        appointmentListAdapter = new AppointmentListAdapter(GroupViewActivity.this, group, appointments);
        ListView appointmentView = findViewById(R.id.groupview_appointment_list);
        if (appointmentView != null) {
            appointmentView.setAdapter(appointmentListAdapter);
        }

        appointmentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String appointmentUid = appointments.get(position).getUid();
                Intent intent = new Intent(GroupViewActivity.this, AppointmentInfoActivity.class);
                intent.putExtra(Constants.APPOINTMENT_ID, appointmentUid);
                intent.putExtra(Constants.GROUP_ID, group.getUid());
                startActivity(intent);
            }
        });


        // fab to create new Appointment
        FloatingActionButton fab = findViewById(R.id.groupview_create_activity);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupViewActivity.this, CreateAppointmentActivity.class);
                intent.putExtra(Constants.GROUP_ID, group.getUid());
                startActivity(intent);
            }
        });

        // fab to add users to group
        FloatingActionButton fab2 = findViewById(R.id.groupview_add_user);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupViewActivity.this, AddUserActivity.class);
                intent.putExtra(Constants.GROUP_ID, group.getUid());
                startActivity(intent);
            }
        });
    }

    /**
     * Prepares the Content by loading data from the {@link FirebaseDatabase}.
     */
    private void prepareContent() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            groupID = String.valueOf(extras.get(Constants.GROUP_ID));
            if (groupID != null) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID);
                new GroupLoadListener(group, this, reference);

                Query appointmentsRef = FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).orderByChild(Constants.GROUP_ID).equalTo(groupID);
                new AppointmentLoadListener(appointments, appointmentListAdapter, this, appointmentsRef);

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_change_name: // change the group name
                DialogFragment dialogFragment = new ChangeGroupNameDialog();
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.GROUP, group);
                dialogFragment.setArguments(bundle);
                dialogFragment.show(this.getFragmentManager(), Constants.CHANGE_GROUP_NAME);
                return true;

            case R.id.menu_group_infos: //Get information about the group
                Intent intent = new Intent(this, GroupInfoActivity.class);
                intent.putExtra(Constants.GROUP_ID, groupID);
                intent.putExtra(Constants.GROUP, group);
                startActivity(intent);
                return true;

            case R.id.menu_leave_group: //Leave the group (only non admin)
                DialogFragment leaveGroupDialogFragment = new LeaveGroupDialog();
                leaveGroupDialogFragment.show(getFragmentManager(), Constants.LEAVE_GROUP_DIALOG);
                return true;

            case R.id.menu_delete_group: //Delete the group (only admin)
                DialogFragment deleteGroupFragment = new DeleteGroupDialog();
                deleteGroupFragment.show(getFragmentManager(), Constants.DELETE_GROUP_DIALOG);
                return true;

            case R.id.menu_expired_appointments:
                Intent expired = new Intent(this, ExpiredAppointmentsActivity.class);
                expired.putExtra(Constants.GROUP_ID, groupID);
                startActivity(expired);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public UserListAdapter getUserListAdapter() {
        return userListAdapter;
    }

    @Override
    public void leaveGroupOnDialogPositiveClick(DialogFragment dialog) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDao.leaveGroup(currentUser.getUid(), groupID);
        Intent intent = new Intent(GroupViewActivity.this, DashboardActivity.class);
        intent.putExtra(Constants.USER_ID, currentUser.getUid());
        startActivity(intent);
    }

    @Override
    public void leaveGroupOnDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        restart();
        prepareUI();
        prepareContent();
    }

    /**
     * rebuilds the {@link android.text.Layout}
     */
    private void restart() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null && group != null) {
            boolean isAdmin = group.isAdmin(currentUser.getUid());
            setAdmin(isAdmin);
            onCreateOptionsMenu(this.menu);
        }
        appointments.clear();
        users.clear();
    }


    @Override
    public void deleteGroupOnDialogPositiveClick(DialogFragment dialog) {
        FirebaseDao.deleteGroup(users, groupID, appointments);
        Intent openDashboardActivity = new Intent(GroupViewActivity.this, edu.passau.uni.de.letsgobuddy.domain.activities.DashboardActivity.class);
        openDashboardActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityIfNeeded(openDashboardActivity, 0);
    }

    @Override
    public void deleteGroupOnDialogNegativeClick(DialogFragment dialog) {

    }
}

