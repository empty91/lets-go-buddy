package edu.passau.uni.de.letsgobuddy;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Locale;

import edu.passau.uni.de.letsgobuddy.domain.activities.AppointmentInfoActivity;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Message;

/**
 * {@link com.google.firebase.messaging.FirebaseMessagingService} to send {@link com.google.firebase.messaging.FirebaseMessaging messages}.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private String TAG = "FirebaseMessaging";

    /**
     * Sends a {@link Message} to the {@link DatabaseReference}.
     *
     * @param groupID       the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Group#uid}.
     * @param textDe        the {@link Message#textDe} in {@link Locale#GERMANY}.
     * @param textEn        the {@link Message#textEn} in {@link Locale#ENGLISH}.
     * @param appointmentID the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment#uid}.
     */
    public static void sendMessage(@NonNull String groupID, @NonNull String textDe, @NonNull String textEn, @NonNull String appointmentID) {

        DatabaseReference messagesRef = FirebaseDatabase.getInstance().getReference().child(Constants.MESSAGES).child(groupID);
        DatabaseReference messageRef = messagesRef.push();
        Message msg = new Message(messageRef.getKey(), appointmentID, textDe, textEn);
        messageRef.setValue(msg);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage);
        } else {
            Log.e(TAG, "Message had no data");
        }
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message body received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, AppointmentInfoActivity.class);
        String groupID = remoteMessage.getData().get(Constants.GROUP_ID);
        String appointmentID = remoteMessage.getData().get(Constants.APPOINTMENT_ID);
        if (groupID != null & appointmentID != null) {

            intent.putExtra(Constants.APPOINTMENT_ID, appointmentID);
            intent.putExtra(Constants.GROUP_ID, groupID);

        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "0";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Locale current = getResources().getConfiguration().locale;
        String message;
        if (current.equals(Locale.GERMANY)) {
            message = remoteMessage.getData().get(Constants.TEXT_DE);
        } else {
            message = remoteMessage.getData().get(Constants.TEXT_EN);
        }

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.push_notification)
                        .setContentTitle(remoteMessage.getData().get(Constants.TITLE) != null ? remoteMessage.getData().get(Constants.TITLE) : getString(R.string.app_name))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
