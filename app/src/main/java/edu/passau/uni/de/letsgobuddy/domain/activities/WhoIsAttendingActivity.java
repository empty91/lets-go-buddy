package edu.passau.uni.de.letsgobuddy.domain.activities;

import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.LinkedList;
import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.adapters.AttendingListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.WhoIsAttendingListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * This activity shows all members of the {@link Group}
 * and whether they are attending an {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment}
 * or not.
 */
public class WhoIsAttendingActivity extends BaseActivity {

    private List<User> members = new LinkedList<>();
    private List<User> users = new LinkedList<>();
    private Group group = new Group();
    private boolean expired = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.who_is_attending_activity);
        prepareUI();
        prepareContent();
    }

    /**
     * Prepares the Content by loading data from the {@link FirebaseDatabase}.
     */
    private void prepareContent() {

        expired = getIntent().getExtras().getBoolean(Constants.EXPIRED);
        String groupID = getIntent().getExtras().getString(Constants.GROUP_ID);
        String appointmentID = getIntent().getExtras().getString(Constants.APPOINTMENT_ID);

        ListView membersView = (ListView) findViewById(R.id.members);
        AttendingListAdapter attendingListAdapter = new AttendingListAdapter(WhoIsAttendingActivity.this, group, users, appointmentID, expired);
        membersView.setAdapter(attendingListAdapter);

        DatabaseReference groupReference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID);
        new WhoIsAttendingListener(group, users, attendingListAdapter, this, groupReference);
    }

    /**
     * Prepares the {@link android.text.Layout}
     */
    private void prepareUI() {
        hideLoadingScreen();
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
