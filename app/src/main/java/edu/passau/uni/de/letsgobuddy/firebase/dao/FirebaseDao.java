package edu.passau.uni.de.letsgobuddy.firebase.dao;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import edu.passau.uni.de.letsgobuddy.MyFirebaseMessagingService;
import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.activities.AddUserActivity;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.domain.util.DateFormatUtil;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * Dao to perform {@link FirebaseDatabase} modifications.
 */
public class FirebaseDao {

    /**
     * Saves a {@link User} to the {@link FirebaseDatabase}.
     *
     * @param firebaseUser the {@link FirebaseAuth#getCurrentUser()}.
     * @return the saved {@link User};
     */
    public static User persistUser(FirebaseUser firebaseUser) {

        User user = new User(firebaseUser.getUid(), firebaseUser.getDisplayName(), firebaseUser.getEmail());
        FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(firebaseUser.getUid()).setValue(user);
        return user;
    }

    /**
     * Saves a {@link Group}.
     *
     * @param name the {@link Group#name}.
     * @return the saved {@link Group}.
     */
    public static Group persistGroup(final String name) {

        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        Group group = null;
        if (currentUser != null) {

            // push new Id to groups node
            DatabaseReference groupsReference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS);
            DatabaseReference groupRef = groupsReference.push();
            String pushedKey = groupRef.getKey();

            // initially the Group only contains one member
            Map<String, String> groupUsers = new HashMap<>();
            groupUsers.put(currentUser.getUid(), currentUser.getUid());
            group = new Group(pushedKey, name, currentUser.getUid(), groupUsers, null);

            // save group to the user Node
            DatabaseReference userReference = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(currentUser.getUid()).child(Constants.GROUPS);
            Map<String, Object> userGroups = new HashMap<>();

            userGroups.put("/" + pushedKey, pushedKey);
            userReference.updateChildren(userGroups);

            groupRef.setValue(group);
        }
        return group;
    }

    /**
     * Changes the {@link Group#name}.
     *
     * @param group the {@link Group} to change.
     * @return the changed {@link Group}.
     */
    public static Group changeGroupName(Group group) {

        Map<String, Object> updates = new HashMap<>();
        updates.put(Constants.NAME, group.getName());

        DatabaseReference groupReference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(group.getUid());
        groupReference.updateChildren(updates);

        return group;
    }

    /**
     * Saves an {@link Appointment}.
     *
     * @param group       the {@link Group} the {@link Appointment} belongs to.
     * @param appointment the {@link Appointment} to save.
     * @param context     the {@link Context}.
     * @return the saved {@link Appointment}
     */
    public static Appointment persistAppointment(Group group, Appointment appointment, Context context) {

        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser != null) {

            // first save appointment
            DatabaseReference appointmentReference = FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS);
            DatabaseReference appointmentsRef = appointmentReference.push();
            String pushedKey = appointmentsRef.getKey();
            appointment.setUid(pushedKey);
            appointmentsRef.setValue(appointment);

            // update Group
            DatabaseReference groupReference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(group.getUid()).child(Constants.APPOINTMENTS);
            Map<String, Object> groupAppointments = new HashMap<>();
            groupAppointments.put("/" + pushedKey, appointment.getUid());
            groupReference.updateChildren(groupAppointments);


        }
        sendMessage(context, appointment, group);

        return appointment;
    }

    /**
     * Sends a new {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Message}
     *
     * @param context     the {@link Context}.
     * @param appointment the {@link Appointment} the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Message} belongs to.
     * @param group       the {@link Group} the {@link Appointment} belongs to.
     */
    private static void sendMessage(Context context, Appointment appointment, Group group) {
        String newAppointmentDe = getLocaleStringResource(Locale.GERMANY, R.string.new_appointment, context);
        String atDe = getLocaleStringResource(Locale.GERMANY, R.string.new_appointment_time, context);

        String newAppointmentEn = getLocaleStringResource(Locale.ENGLISH, R.string.new_appointment, context);
        String atEn = getLocaleStringResource(Locale.ENGLISH, R.string.new_appointment_time, context);

        DateFormatUtil.formatDate(Locale.GERMANY, new Date(appointment.getDateAndTime()));

        String textDe = newAppointmentDe + " " + group.getName() + " " + atDe + " " + DateFormatUtil.formatDate(Locale.GERMANY, new Date(appointment.getDateAndTime()));
        String textEn = newAppointmentEn + " " + group.getName() + " " + atEn + " " + DateFormatUtil.formatDate(Locale.ENGLISH, new Date(appointment.getDateAndTime()));
        MyFirebaseMessagingService.sendMessage(group.getUid(), textDe, textEn, appointment.getUid());
    }

    /**
     * Returns a String value from {@link R} in the requested Locale.
     *
     * @param requestedLocale the {@link Locale}.
     * @param resourceId      the id of the String.
     * @param context         the {@link Context}.
     * @return the value of the String in given {@link Locale}.
     */
    private static String getLocaleStringResource(Locale requestedLocale, int resourceId, Context context) {
        String result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) { // use latest api
            Configuration config = new Configuration(context.getResources().getConfiguration());
            config.setLocale(requestedLocale);
            result = context.createConfigurationContext(config).getText(resourceId).toString();
        } else { // support older android versions
            Resources resources = context.getResources();
            Configuration conf = resources.getConfiguration();
            Locale savedLocale = conf.locale;
            conf.locale = requestedLocale;
            resources.updateConfiguration(conf, null);

            // retrieve resources from desired locale
            result = resources.getString(resourceId);

            // restore original locale
            conf.locale = savedLocale;
            resources.updateConfiguration(conf, null);
        }

        return result;
    }


    /**
     * Adds a {@link User} to a {@link Group}.
     *
     * @param email    the {@link User#email} to add.
     * @param groupID  the {@link Group#uid} to add to.
     * @param activity the calling {@link BaseActivity}
     */
    public static void addUser(final String email, final String groupID, final BaseActivity activity) {

        //search for user with given email
        if (activity instanceof AddUserActivity) {
            Query userRef = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).orderByChild(Constants.EMAIL).equalTo(email);
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // iterate matching users (should only be one)

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User user = snapshot.getValue(User.class);
                        if (user != null && user.getEmail().equals(email)) { // correct user found
                            if (user.getGroups().containsValue(groupID)) { // already member of group
                                ((AddUserActivity) activity).alreadyInGroup();
                                return;
                            } else { // when not in group, add to group
                                addVerifiedUser(user, groupID);
                                ((AddUserActivity) activity).setEditTextBlank();
                                ((AddUserActivity) activity).success();
                                return;
                            }
                        }
                    }
                    ((AddUserActivity) activity).failed();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    /**
     * Adds a {@link User} to a {@link Group}.
     *
     * @param user    the {@link User}
     * @param groupID the {@link Group#uid} to add to.
     */
    private static void addVerifiedUser(final User user, String groupID) {
        //Adds user to groupnode
        DatabaseReference userGroupsRef = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(user.getUid()).child(Constants.GROUPS);
        Map<String, Object> userUpdates = new HashMap<>();
        userUpdates.put(groupID, groupID);
        userGroupsRef.updateChildren(userUpdates);

        //Adds group to Usernode
        DatabaseReference groupUsersRef = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID).child(Constants.USERS);
        Map<String, Object> groupUpdates = new HashMap<>();
        groupUpdates.put(user.getUid(), user.getUid());
        groupUsersRef.updateChildren(groupUpdates);
    }

    /**
     * Makes a {@link User}
     *
     * @param groupID the {@link Group#uid}.
     * @param userID  the {@link User#uid} to be new Admin.
     */
    public static void makeUserAdmin(String groupID, String userID) {
        FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID).child(Constants.ADMIN).setValue(userID);
    }

    /**
     * Kicks a {@link User} from a {@link Group}.
     *
     * @param groupID the {@link Group#uid}.
     * @param userID  the {@link User#uid}.
     */
    public static void kickMember(String groupID, String userID) {
        FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(userID).child(Constants.GROUPS).child(groupID).removeValue();
        FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID).child(Constants.USERS).child(userID).removeValue();
    }

    /**
     * Leaves a {@link Group}
     *
     * @param userID  the {@link User#uid} to leave.
     * @param groupID the {@link Group#uid} to be left
     */
    public static void leaveGroup(String userID, String groupID) {
        FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID).child(Constants.USERS).child(userID).removeValue();
        FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(userID).child(Constants.GROUPS).child(groupID).removeValue();
    }

    /**
     * Deletes a {@link Group}.
     *
     * @param users        the {@link User#uid users} of the {@link Group}.
     * @param groupID      the {@link Group#uid} to delete.
     * @param appointments the {@link Group#appointments} to delete.
     */
    public static void deleteGroup(List<User> users, String groupID, List<Appointment> appointments) {

        FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID).removeValue();
        for (Appointment appointment : appointments) {
            FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointment.getUid()).removeValue();
        }
        for (User user : users) {
            FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(user.getUid()).child(Constants.GROUPS).child(groupID).removeValue();
        }
    }

    /**
     * Cancels an {@link Appointment}.
     *
     * @param groupID       the {@link Appointment#groupID}.
     * @param appointmentID the {@link Appointment#uid}.
     */
    public static void cancelAppointment(String groupID, String appointmentID) {
        FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID).child(Constants.APPOINTMENTS).child(appointmentID).removeValue();
        FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID).removeValue();
    }
}
