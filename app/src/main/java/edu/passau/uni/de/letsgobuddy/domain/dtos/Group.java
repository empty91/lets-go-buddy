package edu.passau.uni.de.letsgobuddy.domain.dtos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Dto to represent a {@link Group} of the {@link android.app.Application}.
 */
@IgnoreExtraProperties
public class Group implements Parcelable {

    // unique ID provided by Firebase
    private String uid;

    // name provided by user input
    private String name;

    // the key of the admin user
    private String admin;

    // foreign keys of users which belong to the group
    private Map<String, String> users = new HashMap<>();

    // foreign keys of appointmens which belong to the group
    private Map<String, String> appointments = new HashMap<>();

    /**
     * Default {@link java.lang.reflect.Constructor}.
     */
    public Group() {
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link User}.
     *
     * @param uid the {@link Group#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     */
    public Group(String uid) {
        this.uid = uid;
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link User}.
     *
     * @param uid   the {@link Group#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     * @param name  the {@link Group#name} provided by the {@link User}.
     * @param admin the {@link Group#admin}.
     */
    public Group(String uid, String name, String admin) {
        this(uid, name, admin, new HashMap<String, String>(), new HashMap<String, String>());
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link User}.
     *
     * @param uid          the {@link Group#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     * @param name         the {@link Group#name} provided by the {@link User}.
     * @param admin        the {@link Group#admin}.
     * @param users        the {@link Group#users} which belong to the {@link Group}.
     * @param appointments the {@link Group#appointments} which belong to the {@link Group}.
     */
    public Group(String uid, String name, String admin, Map<String, String> users, Map<String, String> appointments) {
        this.uid = uid;
        this.name = name;
        this.admin = admin;
        this.users = users;
        this.appointments = appointments;
    }

    /**
     * {@link java.lang.reflect.Constructor} for a {@link Parcelable} {@link Group}.
     *
     * @param in the {@link Parcel} to create a {@link Group} from.
     */
    public Group(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.admin = in.readString();
        in.readMap(users, Map.class.getClassLoader());
        in.readMap(appointments, Map.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(uid);
        parcel.writeString(name);
        parcel.writeString(admin);
        parcel.writeMap(users);
        parcel.writeMap(appointments);
    }

    /**
     * {@link android.os.Parcelable.Creator} for a {@link Group}.
     */
    public static final Creator<Group> CREATOR = new Creator<Group>() {

        /**
         * {@link java.lang.reflect.Constructor} for a {@link Group} from a {@link Parcel}.
         * @param in the {@link Parcel}.
         * @return the {@link Group}.
         */
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        /**
         * {@link java.lang.reflect.Constructor} for a {@link Group} {@link java.lang.reflect.Array}.
         * @param size the {@link java.lang.reflect.Array} size.
         * @return the {@link Group} {@link java.lang.reflect.Array}.
         */
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public Map<String, String> getUsers() {
        return users;
    }

    public void setUsers(Map<String, String> users) {
        this.users = users;
    }

    public Map<String, String> getAppointments() {
        return appointments;
    }

    public void setAppointments(Map<String, String> appointments) {
        this.appointments = appointments;
    }

    /**
     * Checks whether given {@link User#uid} matches the {@link Group#admin}.
     *
     * @param uid the {@link User#uid}.
     * @return true if the {@link User#uid} matches {@link Group#admin}, false otherwise.
     */
    public boolean isAdmin(String uid) {
        return uid.equals(this.getAdmin());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return uid.equals(group.uid) && name.equals(group.name) && admin.equals(group.admin) && users.equals(group.users) && (appointments != null ? appointments.equals(group.appointments) : group.appointments == null);
    }

    @Override
    public int hashCode() {
        int result = uid.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + admin.hashCode();
        result = 31 * result + users.hashCode();
        result = 31 * result + (appointments != null ? appointments.hashCode() : 0);
        return result;
    }
}
