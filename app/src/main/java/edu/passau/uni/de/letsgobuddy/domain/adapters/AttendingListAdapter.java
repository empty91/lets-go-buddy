package edu.passau.uni.de.letsgobuddy.domain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Attendant;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;

/**
 * {@link ArrayAdapter} to display all {@link Attendant attendants} for an {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment}.
 */
public class AttendingListAdapter extends ArrayAdapter<User> {

    private Context context;
    private Group group;
    private List<User> users;
    private String appointmentID;
    private boolean expired = false;

    /**
     * {@link java.lang.reflect.Constructor} for an {@link AttendingListAdapter}.
     *
     * @param context       the {@link Context}.
     * @param group         the {@link Group}.
     * @param users         the {@link User users}.
     * @param expired       true, if {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment} is over, false otherwise
     * @param appointmentID the {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment#uid}.
     */
    public AttendingListAdapter(@NonNull Context context, @NonNull Group group, @NonNull List<User> users, String appointmentID, boolean expired) {
        super(context, R.layout.groupview_user_items_fragment, users);
        this.context = context;
        this.group = group;
        this.users = users;
        this.appointmentID = appointmentID;
        this.expired = expired;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi != null ? vi.inflate(R.layout.whoisattending_user_item_fragment, null) : null;
        }

        if (convertView != null) {
            String userUid = users.get(position).getUid();
            // build reference to user
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(userUid);
            final View finalConvertView = convertView;
            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final User user = dataSnapshot.getValue(User.class);
                    if (user != null) { // get User and display name
                        TextView userName = finalConvertView.findViewById(R.id.user_item_name);
                        final ImageView attendingView = finalConvertView.findViewById(R.id.user_attending_image);
                        if (user.isCurrentUser()) {
                            userName.setText(context.getText(R.string.you));
                        } else {
                            userName.setText(user.getName());
                        }

                        // build reference to attending users of appointment
                        DatabaseReference attendingRef = FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID).child(Constants.MEMBERS).child(user.getUid()).child(Constants.ATTENDING);
                        attendingRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Boolean isAttending = dataSnapshot.getValue(Boolean.class); // check attending
                                if (isAttending != null) {
                                    if (isAttending)
                                        if (expired) {
                                            attendingView.setImageResource(R.drawable.green_icon2);
                                        } else {
                                            attendingView.setImageResource(R.drawable.green_icon);
                                        }
                                    else {
                                        if (expired) {
                                            attendingView.setImageResource(R.drawable.red_icon2);
                                        } else {
                                            attendingView.setImageResource(R.drawable.red_icon);
                                        }
                                    }
                                } else {
                                    /*
                                    The user is not yet in the attending list because the appointment was created before the user was added to the group, so add him
                                     */
                                    FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID).child(Constants.MEMBERS).child(user.getUid()).setValue(new Attendant(user.getUid(), false));
                                    if (expired) {
                                        attendingView.setImageResource(R.drawable.red_icon2);
                                    } else {
                                        attendingView.setImageResource(R.drawable.red_icon);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }

        return convertView;

    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}

