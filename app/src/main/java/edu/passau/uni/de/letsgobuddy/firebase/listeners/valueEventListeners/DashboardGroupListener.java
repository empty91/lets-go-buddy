package edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.passau.uni.de.letsgobuddy.domain.adapters.GroupListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;


/**
 * {@link FirebaseValueListener} to listen on {@link Group groups} of a {@link edu.passau.uni.de.letsgobuddy.domain.dtos.User}.
 */
public class DashboardGroupListener extends FirebaseValueListener {

    private List<Group> groups;

    // register to keep track of groups
    private Map<String, Integer> groupRegister;
    private GroupListAdapter groupListAdapter;
    private Map<String, GroupChildListener> groupChildListeners = new HashMap<>();

    // map to keep track of database references
    private Map<String, DatabaseReference> referenceMap = new HashMap<>();

    /**
     * {@link java.lang.reflect.Constructor} for a {@link DashboardGroupListener}.
     *
     * @param groups           the {@link List} of {@link Group groups} to fill.
     * @param groupRegister    the {@link Map} to hold the indizes of the {@link Group groups}.
     * @param groupListAdapter the {@link GroupListAdapter} to notify.
     * @param activity         the {@link BaseActivity} the {@link DashboardGroupListener} is registered to.
     * @param reference        the {@link DatabaseReference}.
     */
    public DashboardGroupListener(List<Group> groups, Map<String, Integer> groupRegister, GroupListAdapter groupListAdapter, BaseActivity activity, DatabaseReference reference) {
        super(reference, activity);
        this.groups = groups;
        this.groupRegister = groupRegister;
        this.groupListAdapter = groupListAdapter;
    }

    /**
     * Adds a new {@link GroupChildListener} to the {@link DatabaseReference reference}
     *
     * @param reference the {@link DatabaseReference}
     */
    private void addGroupChildListener(DatabaseReference reference) {

        // get current listener from map
        GroupChildListener oldListener = groupChildListeners.get(reference.getKey());
        if (oldListener != null) { // remove old listener if already present
            reference.removeEventListener(oldListener);
        }

        // add new Listener
        GroupChildListener newListener = new GroupChildListener(reference);
        groupChildListeners.put(reference.getKey(), newListener);
        referenceMap.put(reference.getKey(), reference);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        activity.showLoadingScreen();

        // get all children from the snapshot
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            // if the reference already exists, get the existing, else create a new reference
            DatabaseReference groupIDNode = referenceMap.get(snapshot.getKey()) != null ? referenceMap.get(snapshot.getKey()) : FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(snapshot.getKey());
            addGroupChildListener(groupIDNode);
        }
        activity.hideLoadingScreen();
    }


    @Override
    public void onSignOut() {
        super.onSignOut();
        for (GroupChildListener groupChildListener : groupChildListeners.values()) {
            groupChildListener.onSignOut();
        }
    }

    /**
     * {@link com.google.firebase.database.ValueEventListener} to check the children of the Group Node.
     */
    private class GroupChildListener extends FirebaseValueListener {

        /**
         * {@link java.lang.reflect.Constructor} for a {@link GroupChildListener}.
         *
         * @param reference the {@link DatabaseReference}.
         */
        GroupChildListener(DatabaseReference reference) {
            super(reference);
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Group group = dataSnapshot.getValue(Group.class);
            String key = dataSnapshot.getKey();
            if (group != null) {

                // group does not exist
                if (groupRegister.get(key) == null) {

                    // add group to list and register
                    groups.add(group);
                    groupRegister.put(key, groups.indexOf(group));
                } else { // group exists

                    // readd
                    int index = groupRegister.get(key);
                    groups.remove(index);
                    groups.add(index, group);
                }
            } else { // no group fetched, remove the key from register and group
                Integer index = groupRegister.get(key);
                groupRegister.remove(key);
                if (index != null && index < groups.size()) {
                    groups.remove(index.intValue());
                    for (Group groupItem : groups) {
                        groupRegister.put(groupItem.getUid(), groups.indexOf(groupItem));
                    }
                }
            }
            groupListAdapter.notifyDataSetChanged();
        }
    }
}

