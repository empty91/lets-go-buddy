package edu.passau.uni.de.letsgobuddy.firebase.listeners.singleEventListeners;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;


/**
 * {@link FirebaseValueListener} to load a {@link User}. If the {@link User} does not exist a new {@link User} is created.
 */
public class UserLoadListener extends FirebaseValueListener {


    /**
     * {@link java.lang.reflect.Constructor} for a {@link UserLoadListener}.
     *
     * @param reference the {@link DatabaseReference}.
     */
    public UserLoadListener(DatabaseReference reference) {
        super(reference, true);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        User user = dataSnapshot.getValue(User.class);
        if (user == null) {
            FirebaseDao.persistUser(FirebaseAuth.getInstance().getCurrentUser());
        }
    }
}