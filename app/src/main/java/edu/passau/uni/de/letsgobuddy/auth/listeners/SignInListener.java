package edu.passau.uni.de.letsgobuddy.auth.listeners;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.auth.activities.LoginActivity;
import edu.passau.uni.de.letsgobuddy.auth.exceptions.NoAccessGrantedException;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.singleEventListeners.UserLoadListener;


/**
 * {@link View.OnClickListener} to handle {@link FirebaseAuth#signInWithCredential(AuthCredential)}.
 */
public class SignInListener implements View.OnClickListener {

    /**
     * The parent {@link LoginActivity}.
     */
    private LoginActivity loginActivity;


    // login requestCode
    public static int RC_SIGN_IN = 1;


    public SignInListener(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                //Hiding the intro screen
                loginActivity.hideImage();
                break;
        }
    }

    /**
     * Starts the {@link GoogleSignInClient#getSignInIntent()}.
     */
    private void signIn() {

        try {

            // check if access already granted, navigate directly to Dashboard without starting the signInIntent
            if (loginActivity.getGoogleSignInClient().silentSignIn().isComplete()) {
                GoogleSignInAccount result = loginActivity.getGoogleSignInClient().silentSignIn().getResult(NoAccessGrantedException.class);
                loginActivity.showLoadingScreen();
                firebaseAuthWithGoogle(result);
            } else {
                throw new NoAccessGrantedException(Constants.SILENT_SIGN_IN_ERROR);
            }
        } catch (NoAccessGrantedException e) {

            // if access has not yet been granted, start signInIntent
            startSignInActivity();
        }
    }

    /**
     * Starts the {@link android.app.Activity} for the {@link GoogleSignInClient}.
     */
    private void startSignInActivity() {
        Intent signInIntent = loginActivity.getGoogleSignInClient().getSignInIntent();
        loginActivity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Processes the result of the {@link GoogleSignInClient#getSignInIntent()}.
     *
     * @param requestCode the requestCode
     * @param resultCode  the resultCode
     * @param data        the {@link Intent}
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                loginActivity.showLoadingScreen();
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {

                // Google Sign In failed - show Snackbar
                Log.e(SignInListener.class.getSimpleName(), Constants.SIGN_IN_ERROR, e);
                showError();
            }
        }
    }

    /**
     * Shows error {@link Snackbar} and removes the {@link ProgressBar}.
     */
    private void showError() {

        loginActivity.hideLoadingScreen();
        // Make the intro screen visible again
        loginActivity.showImage();
        Snackbar snackbar = Snackbar.make(loginActivity.findViewById(android.R.id.content), R.string.login_fail, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * Authenticates the {@link GoogleSignInAccount} with {@link FirebaseAuth}.
     *
     * @param account the {@link GoogleSignInAccount}
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        loginActivity.getFirebaseAuth().signInWithCredential(credential)
                .addOnCompleteListener(new SignInListenerHelper());
    }

    /**
     * Helper class to handle {@link FirebaseAuth#signInWithCredential(AuthCredential)}.
     */
    private class SignInListenerHelper implements OnCompleteListener<AuthResult> {

        @Override
        public void onComplete(@NonNull Task task) {

            if (task.isSuccessful()) {

                FirebaseUser user = loginActivity.getFirebaseAuth().getCurrentUser();
                if (user != null) {

                    Log.d(this.getClass().getSimpleName(), Constants.SIGN_IN_SUCCESS + user.getUid());
                    DatabaseReference userIDNode = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(user.getUid());
                    new UserLoadListener(userIDNode);
                    loginActivity.goToDashboard(user.getUid());
                } else {

                    Log.e(this.getClass().getSimpleName(), Constants.SIGN_IN_ERROR, new IllegalStateException(Constants.USER_NULL));
                    showError();
                }
            } else {

                Log.e(this.getClass().getSimpleName(), Constants.SIGN_IN_ERROR, task.getException());
                showError();
            }
        }
    }
}
