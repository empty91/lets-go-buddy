package edu.passau.uni.de.letsgobuddy.domain.dialogs;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import edu.passau.uni.de.letsgobuddy.R;

public class LeaveGroupDialog extends DialogFragment {

    public interface NoticeDialogListener {
        public void leaveGroupOnDialogPositiveClick(DialogFragment dialog);

        public void leaveGroupOnDialogNegativeClick(DialogFragment dialog);
    }

    NoticeDialogListener mListener;

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle SavedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.leave_group_title));
        builder.setMessage(getResources().getString(R.string.leave_group_text));
        builder.setPositiveButton(getResources().getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mListener.leaveGroupOnDialogPositiveClick(LeaveGroupDialog.this);
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mListener.leaveGroupOnDialogNegativeClick(LeaveGroupDialog.this);
            }
        });
        return builder.create();
    }
}
