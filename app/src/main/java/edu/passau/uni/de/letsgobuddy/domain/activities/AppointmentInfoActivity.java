package edu.passau.uni.de.letsgobuddy.domain.activities;

import android.app.DialogFragment;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.CancelAppointmentDialog;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Appointment;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Attendant;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.domain.util.DateFormatUtil;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.singleEventListeners.GroupLoadSingleListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * This {@link android.app.Activity} is used to display the information about an {@link Appointment}.
 */
public class AppointmentInfoActivity extends BaseActivity implements CancelAppointmentDialog.NoticeDialogListener {

    private String appointmentID;
    private Appointment appointment;
    private String groupID;
    private Group group;
    private FirebaseUser currentUser;
    private User creator;
    private Attendant currentViewer;
    private boolean expired = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.appointment_info_activity);
        prepareContent();

    }

    /**
     * sets color for the {@link Appointment} depending on whether the {@link User} is attending or not
     */
    private void setIndicatorColor() {
        String userID = currentUser.getUid();
        final ImageView indicator = findViewById(R.id.attend_indicator);
        DatabaseReference attendantRef = FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID).child(Constants.MEMBERS).child(userID);
        attendantRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentViewer = dataSnapshot.getValue(Attendant.class);
                if (currentViewer != null) {
                    boolean isAttending = currentViewer.isAttending();
                    if (isAttending) {
                        if (expired) {
                            indicator.setImageResource(R.drawable.green_icon2);
                        } else
                            indicator.setImageResource(R.drawable.green_icon);
                    } else {
                        if (expired) {
                            indicator.setImageResource(R.drawable.red_icon2);
                        }
                        indicator.setImageResource(R.drawable.red_icon);
                    }
                } else {
                    indicator.setImageResource(R.drawable.red_icon);
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        hideLoadingScreen();
    }

    /**
     * Prepares the Content by loading data from the {@link FirebaseDatabase}.
     */
    private void prepareContent() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            appointmentID = extras.getString(Constants.APPOINTMENT_ID);
            groupID = extras.getString(Constants.GROUP_ID);
            expired = extras.getBoolean(Constants.EXPIRED);
        }

        // loads the group once
        DatabaseReference groupRef = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID);
        new GroupLoadSingleListener(groupRef, this);

        // load appointment
        final DatabaseReference appointmentRef = FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID);
        appointmentRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                appointment = dataSnapshot.getValue(Appointment.class);
                if (appointment != null && appointment.isSet()) {

                    // sets correct menu
                    AppointmentInfoActivity.this.setAdmin(group.isAdmin(FirebaseAuth.getInstance().getCurrentUser().getUid()) || appointment.getCreatedBy().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()));

                    // Displays the Appointment information correctly
                    TextView appointmentName = findViewById(R.id.appointment_name_view);
                    final TextView appointmentDate = findViewById(R.id.appointment_time_view);
                    TextView appointmentPlace = findViewById(R.id.appointment_place_view);


                    if (expired) {
                        appointmentName.setText(appointment.getName() + " (" + getResources().getString(R.string.expired) + ")");
                    } else {
                        appointmentName.setText(appointment.getName());
                    }
                    Calendar c = new GregorianCalendar();
                    c.setTime(new Date(appointment.getDateAndTime()));

                    String appointmentDateText = DateFormatUtil.formatDate(AppointmentInfoActivity.this, c, DateFormat.SHORT) + " " + DateFormatUtil.formatTime(c);
                    appointmentDate.setText(appointmentDateText);

                    try {
                        appointmentPlace.setText(getAddress(Double.parseDouble(appointment.getLatitude()), Double.parseDouble(appointment.getLongitude())));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // get creator
                    String creatorID = appointment.getCreatedBy();
                    DatabaseReference creatorRef = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(creatorID);
                    creatorRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            creator = dataSnapshot.getValue(User.class);
                            TextView creatorView = (TextView) findViewById(R.id.appointment_createdby_view);
                            String creatorViewText = getResources().getString(R.string.created_by) + " " + creator.getName();
                            creatorView.setText(creatorViewText);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    // open google maps
                    ImageView image = findViewById(R.id.place_image);
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String uri = "http://maps.google.com/maps?daddr=" + appointment.getLatitude() + "," + appointment.getLongitude();
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            intent.setPackage("com.google.android.apps.maps");
                            startActivity(intent);
                        }
                    });
                    setIndicatorColor();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        Button attend = findViewById(R.id.button_attend);
        Button dismiss = findViewById(R.id.button_dismiss);

        // attend to appointment
        attend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID).child(Constants.MEMBERS).child(currentUser.getUid()).setValue(new Attendant(currentUser.getUid(), true));
                Toast.makeText(AppointmentInfoActivity.this, getResources().getString(R.string.attending), Toast.LENGTH_SHORT).show();
                setIndicatorColor();
            }
        });
// dismiss appointment
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase.getInstance().getReference().child(Constants.APPOINTMENTS).child(appointmentID).child(Constants.MEMBERS).child(currentUser.getUid()).setValue(new Attendant(currentUser.getUid(), false));
                Toast.makeText(AppointmentInfoActivity.this, getResources().getString(R.string.dismissing), Toast.LENGTH_SHORT).show();
                setIndicatorColor();
            }
        });

        if (expired) {
            attend.setClickable(false);
            dismiss.setClickable(false);
        }
    }


    /**
     * Returns a {@link String} representation of an Address from latitude and longitude.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     * @return a {@link String} representing an Address.
     * @throws IOException if the network is unavailable or any other I/O problem occurs
     */
    private String getAddress(double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1);

        if (!addresses.isEmpty()) {
            String city = addresses.get(0).getLocality();
            String postalCode = addresses.get(0).getPostalCode();
            String thoroughfare = addresses.get(0).getThoroughfare();
            String subthoroughfare = addresses.get(0).getSubThoroughfare();
            if (city != null && postalCode != null && thoroughfare != null && subthoroughfare != null) {
                return thoroughfare + " " + subthoroughfare + ", " + postalCode + " " + city;
            }
        }
        return getResources().getString(R.string.unknown_location);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.menu_show_location:
                String uri = "http://maps.google.com/maps?daddr=" + appointment.getLatitude() + "," + appointment.getLongitude();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                return true;

            case R.id.menu_cancel_appointment:
                DialogFragment cancelappointmentDialogFragment = new CancelAppointmentDialog();
                cancelappointmentDialogFragment.show(getFragmentManager(), Constants.LEAVE_GROUP_DIALOG);
                return true;

            case R.id.menu_attending:
                Intent attending = new Intent(AppointmentInfoActivity.this, WhoIsAttendingActivity.class);
                attending.putExtra(Constants.GROUP_ID, groupID);
                attending.putExtra(Constants.APPOINTMENT_ID, appointmentID);
                attending.putExtra(Constants.EXPIRED, expired);
                startActivity(attending);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void cancelAppointmentOnDialogPositiveClick(DialogFragment dialog) {
        FirebaseDao.cancelAppointment(groupID, appointmentID);
        Intent intent = new Intent(AppointmentInfoActivity.this, GroupViewActivity.class);
        intent.putExtra(Constants.GROUP_ID, groupID);
        startActivity(intent);
    }

    @Override
    public void cancelAppointmentOnDialogNegativeClick(DialogFragment dialog) {

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        prepareContent();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}


