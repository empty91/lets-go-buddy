package edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import edu.passau.uni.de.letsgobuddy.domain.activities.GroupViewActivity;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.generic.FirebaseValueListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * {@link FirebaseValueListener} to load a {@link edu.passau.uni.de.letsgobuddy.domain.dtos.Group}.
 */
public class GroupLoadListener extends FirebaseValueListener {

    private Group group;

    /**
     * {@link java.lang.reflect.Constructor} for a {@link GroupLoadListener}.
     *
     * @param group     the {@link Group} to listen on.
     * @param activity  the {@link BaseActivity} the {@link GroupLoadListener} is registered to.
     * @param reference
     */
    public GroupLoadListener(Group group, BaseActivity activity, DatabaseReference reference) {
        super(reference, activity);
        this.group = group;
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        activity.showLoadingScreen();
        group = dataSnapshot.getValue(Group.class);
        if (group != null) { // group exists
            if (activity instanceof GroupViewActivity) {

                // set admin privileges
                activity.setAdmin(group.isAdmin(FirebaseAuth.getInstance().getCurrentUser().getUid()));
                ((GroupViewActivity) activity).setGroup(group);

                if (activity.getSupportActionBar() != null) { // set title
                    activity.getSupportActionBar().setTitle(group.getName());
                }
            }
        }
        activity.hideLoadingScreen();
    }
}

