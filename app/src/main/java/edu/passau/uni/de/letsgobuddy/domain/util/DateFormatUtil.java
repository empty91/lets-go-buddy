package edu.passau.uni.de.letsgobuddy.domain.util;

import android.content.Context;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Utility class to format a {@link java.util.Date}.
 */
public class DateFormatUtil {

    /**
     * Formats the {@link java.util.Date}.
     *
     * @param context    the {@link Context}.
     * @param calendar   the {@link Calendar} to read from.
     * @param dateFormat the {@link DateFormat} to format in.
     * @return the formatted {@link java.util.Date} as a {@link String}.
     */
    public static String formatDate(Context context, Calendar calendar, int dateFormat) {

        DateFormat df;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            df = DateFormat.getDateInstance(dateFormat, context.getResources().getConfiguration().getLocales().get(0));
        } else {
            df = DateFormat.getDateInstance(dateFormat, context.getResources().getConfiguration().locale);
        }

        return df.format(calendar.getTime());
    }

    /**
     * Formats the {@link java.util.Date}.
     *
     * @param context  the {@link Context}.
     * @param calendar the {@link Calendar} to read from.
     * @return the formatted {@link java.util.Date} as a {@link String}.
     */
    public static String formatDate(Context context, Calendar calendar) {
        return formatDate(context, calendar, DateFormat.FULL);
    }

    /**
     * Formats the {@link Date} depending on the {@link Locale}.
     *
     * @param locale the {@link Locale}.
     * @param date   the {@link Date}
     * @return the formatted {@link Date} as a String.
     */
    public static String formatDate(Locale locale, Date date) {
        DateFormat df;
        df = DateFormat.getDateInstance(DateFormat.FULL, locale);
        return df.format(date);
    }

    /**
     * Formates the {@link java.sql.Time}.
     *
     * @return the formatted {@link java.sql.Time} as a {@link String}
     */
    public static String formatTime(Calendar calendar) {

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        String hourString = String.valueOf(hour);
        if (hour < 10) {
            hourString = "0" + hourString;
        }
        String minuteString = String.valueOf(minute);
        if (minute < 10) {
            minuteString = "0" + minuteString;
        }
        return hourString + ":" + minuteString;
    }
}
