package edu.passau.uni.de.letsgobuddy.domain.dtos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Dto to represent an {@link Attendant} of the {@link android.app.Application}.
 * Represents a minimized version of a {@link User} only holding its {@link User#uid} and a boolean flag to check attendance.
 */
@IgnoreExtraProperties
public class Attendant implements Parcelable {

    // // unique ID provided by Firebase
    private String uid;

    // flag to check attendance
    private boolean attending;

    /**
     * Default {@link java.lang.reflect.Constructor}.
     */
    public Attendant() {
    }

    /**
     * {@link java.lang.reflect.Constructor} for an {@link Attendant}.
     *
     * @param uid the {@link Attendant#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     */
    public Attendant(String uid) {
        this.uid = uid;
    }


    /**
     * {@link java.lang.reflect.Constructor} for an {@link Attendant}.
     *
     * @param uid       the {@link Attendant#uid} provided by {@link com.google.firebase.database.FirebaseDatabase}.
     * @param attending flag to check attendance.
     */
    public Attendant(String uid, boolean attending) {
        this.uid = uid;
        this.attending = attending;
    }

    /**
     * {@link java.lang.reflect.Constructor} for an {@link Parcelable} {@link Attendant}.
     *
     * @param in the {@link Parcel} to create an {@link Attendant} from.
     */
    public Attendant(Parcel in) {
        this.uid = in.readString();
        this.attending = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeByte((byte) (attending ? 1 : 0));
    }

    /**
     * {@link android.os.Parcelable.Creator} for an {@link Attendant}.
     */
    public static final Creator<Attendant> CREATOR = new Creator<Attendant>() {

        /**
         * {@link java.lang.reflect.Constructor} for an {@link Attendant} from a {@link Parcel}.
         * @param in the {@link Parcel}.
         * @return the {@link Attendant}.
         */
        public Attendant createFromParcel(Parcel in) {
            return new Attendant(in);
        }

        /**
         * {@link java.lang.reflect.Constructor} for an {@link Attendant} {@link java.lang.reflect.Array}.
         * @param size the {@link java.lang.reflect.Array} size.
         * @return the {@link Attendant} {@link java.lang.reflect.Array}.
         */
        public Attendant[] newArray(int size) {
            return new Attendant[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isAttending() {
        return attending;
    }

    public void setAttending(boolean attending) {
        this.attending = attending;
    }
}
