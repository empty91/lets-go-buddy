package edu.passau.uni.de.letsgobuddy.auth.listeners;

import android.content.Intent;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.auth.activities.LoginActivity;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * * {@link View.OnClickListener} to handle {@link FirebaseAuth#signOut()}.
 */
public class SignOutListener implements View.OnClickListener {

    /**
     * The parent {@link BaseActivity}.
     */
    private BaseActivity activity;

    /**
     * {@link java.lang.reflect.Constructor} for this {@link android.view.View.OnClickListener}.
     *
     * @param activity the {@link android.app.Activity} this {@link android.view.View.OnClickListener} is registered to.
     */
    public SignOutListener(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.menu_logout:
                signOut();
                break;
        }
    }

    /**
     * Performs the {@link FirebaseAuth#signOut()}.
     */
    public void signOut() {

        // invoke callback
        activity.onSignOut();
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);

        // revoke google access to force new login intent next time
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this.activity, gso);
        client.revokeAccess();
        FirebaseAuth.getInstance().signOut();
    }
}
