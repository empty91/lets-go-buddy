package edu.passau.uni.de.letsgobuddy.domain.activities;


import android.app.DialogFragment;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.LinkedList;
import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.adapters.UserListAdapter;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.KickMemberDialog;
import edu.passau.uni.de.letsgobuddy.domain.dialogs.MakeAdminDialog;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;
import edu.passau.uni.de.letsgobuddy.firebase.dao.FirebaseDao;
import edu.passau.uni.de.letsgobuddy.firebase.listeners.valueEventListeners.GroupInfoListener;
import edu.passau.uni.de.letsgobuddy.generic.activities.BaseActivity;

/**
 * This activity shows all {@link User} of a {@link Group}.
 * Admins can make other users admin or kick members here.
 */
public class GroupInfoActivity extends BaseActivity implements MakeAdminDialog.NoticeDialogListener, KickMemberDialog.NoticeDialogListener {
    private UserListAdapter userListAdapter;
    private List<User> users = new LinkedList<>();
    private Group group = new Group();
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.layoutOnCreate(R.layout.groupinfo_activity);
        prepareUI();
        prepareContent();
    }

    /**
     * Prepares the {@link android.text.Layout}
     */
    private void prepareUI() {


        // init list and listViewAdapter
        userListAdapter = new UserListAdapter(GroupInfoActivity.this, group, users);
        final ListView listView = findViewById(R.id.groupinfo_user_list);
        if (listView != null) {
            listView.setAdapter(userListAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @SuppressWarnings("StatementWithEmptyBody")
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    user = users.get(position);
                    if (!currentUser.getUid().equals(group.getAdmin())) {
                    } else if (user.getUid().equals(currentUser.getUid())) {
                    } else {
                        PopupMenu popup = new PopupMenu(GroupInfoActivity.this, view);
                        popup.getMenuInflater().inflate(R.menu.menu_groupinfo_popup, popup.getMenu());
                        popup.show();

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                switch (menuItem.getItemId()) {
                                    case R.id.menu_make_admin:
                                        DialogFragment makeAdminDialogFragment = new MakeAdminDialog();
                                        makeAdminDialogFragment.show(getFragmentManager(), Constants.MAKE_ADMIN_DIALOG);
                                        return true;

                                    case R.id.menu_kick_member:
                                        DialogFragment kickMemberDialogFragment = new KickMemberDialog();
                                        kickMemberDialogFragment.show(getFragmentManager(), Constants.KICK_MEMBER_DIALOG);
                                        return true;
                                }

                                return false;
                            }
                        });
                    }
                }
            });
        }
    }

    ;

    /**
     * Prepares the Content by loading data from the {@link FirebaseDatabase}.
     */
    private void prepareContent() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String groupID = String.valueOf(extras.get(Constants.GROUP_ID));

            if (groupID != null) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(Constants.GROUPS).child(groupID);
                new GroupInfoListener(group, users, userListAdapter, this, reference);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public UserListAdapter getUserListAdapter() {
        return userListAdapter;
    }


    @Override
    public void makeAdminOnDialogPositiveClick(DialogFragment dialog) {
        FirebaseDao.makeUserAdmin(group.getUid(), user.getUid());
    }

    @Override
    public void makeAdminOnDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    public void kickMemberOnDialogPositiveClick(DialogFragment dialog) {
        FirebaseDao.kickMember(group.getUid(), user.getUid());
        recreate();
        dialog.dismiss();
    }

    @Override
    public void kickMemberOnDialogNegativeClick(DialogFragment dialog) {

    }

}

