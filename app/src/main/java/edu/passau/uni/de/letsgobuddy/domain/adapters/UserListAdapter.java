package edu.passau.uni.de.letsgobuddy.domain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import edu.passau.uni.de.letsgobuddy.R;
import edu.passau.uni.de.letsgobuddy.domain.constants.Constants;
import edu.passau.uni.de.letsgobuddy.domain.dtos.Group;
import edu.passau.uni.de.letsgobuddy.domain.dtos.User;

/**
 * {@link ArrayAdapter} to display the {@link User users} of a {@link Group}.
 */
public class UserListAdapter extends ArrayAdapter<User> {

    private Context context;
    private Group group;
    private List<User> users;

    /**
     * {@link java.lang.reflect.Constructor} for a {@link UserListAdapter}.
     *
     * @param context the {@link Context}.
     * @param group   the {@link Group}.
     * @param users   the {@link User users}.
     */
    public UserListAdapter(@NonNull Context context, @NonNull Group group, @NonNull List<User> users) {
        super(context, R.layout.groupview_user_items_fragment, users);
        this.context = context;
        this.group = group;
        this.users = users;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi != null ? vi.inflate(R.layout.groupview_user_items_fragment, null) : null;
        }

        if (convertView != null) {

            String userUid = users.get(position).getUid();

            // build reference to user
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(Constants.USERS).child(userUid);
            final View finalConvertView = convertView;
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);

                    // fetch user and display informatuon
                    if (user != null) {
                        TextView userName = finalConvertView.findViewById(R.id.user_item_name);
                        if (user.isCurrentUser()) {
                            userName.setText(context.getText(R.string.you));
                        } else {
                            userName.setText(user.getName());
                        }
                        if (!user.isGroupAdmin(group)) {
                            finalConvertView.findViewById(R.id.user_item_admin).setVisibility(View.GONE);
                        } else {
                            finalConvertView.findViewById(R.id.user_item_admin).setVisibility(View.VISIBLE);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return convertView;

    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}

