
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


exports.sendNotification = functions.database.ref('/messages/{groupID}/{messageID}').onWrite((event) => {
    const messageID = event.params.messageID;
    const groupID = event.params.groupID;
    console.log('groupID: ' + groupID);
    console.log('messageID: ' + messageID);

    // If un-follow we exit the function.
    if (!event.data.val()) {
        return console.log('!event.data.val()');
    }

    var path = '/groups/'.concat(groupID).concat('/users');
    var ref = admin.database().ref(path);
    ref.once('value', function (snapshot) {
        var promises = [];

        Object.keys(snapshot.val()).forEach(function (key) {
            promises.push(admin.database().ref('/tokens/' + key + '/token').once('value', function (tokensSnapshot) {
        }))
    });


    return Promise.all(promises).then(resolved => {

        var tokens = [];
        Object.keys(resolved).forEach(function (key) {
            tokens.push(resolved[key].val());
        });
        console.log(tokens);

        admin.database().ref('/messages/'.concat(groupID).concat('/').concat(messageID)).once('value', function (snapshot) {
            console.log(snapshot.val())
            console.log('message textDe: ' + snapshot.val().textDe);
            console.log('message textEn: ' + snapshot.val().textEn);
            console.log('appointment id: ' + snapshot.val().appointmentID);
            const textDe = snapshot.val().textDe;
            const textEn = snapshot.val().textEn;
            const appointmentID = snapshot.val().appointmentID;
            const payload = {
                data: {
                    title: 'Let\'s go Buddy',
                    textDe:  textDe,
                    textEn:  textEn,
                    groupID: groupID,
                    appointmentID: appointmentID
                }
            };
            admin.messaging().sendToDevice(tokens, payload).then(function (response) {
            // See the MessagingDevicesResponse reference documentation for
            // the contents of response.
            console.log('Successfully sent message:', response);
            })
            .catch(function (error) {
                console.log('Error sending message:', error);
            });
        })
    })
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
});


  // Get the list of device notification tokens.
//  var getUsers = [admin.database().ref('/groups/'.concat(groupID).concat('/users')).once('value')];
//  console.log(getUsers);



//  return Promise.all(getUsers).then((results) => {
//    const tokensSnapshot = results;
//    console.log(results);
//
//    // Check if there are any device tokens.
//    if (!tokensSnapshot.hasChildren()) {
//      return console.log('There are no notification tokens to send to.');
//    }
//    console.log('There are', tokensSnapshot.numChildren(), 'tokens to send notifications to.');
//
//    // Notification details.
//    const payload = {
//      notification: {
//        body: 'new notification',
//      },
//    };
//
//    // Listing all tokens.
//    const tokens = Object.keys(tokensSnapshot.val());
//
//    // Send notifications to all tokens.
//    return admin.messaging().sendToDevice(tokens, payload);
//  }).then((response) => {
//    // For each message check if there was an error.
//    const tokensToRemove = [];
//    response.results.forEach((result, index) => {
//      const error = result.error;
//      if (error) {
//        console.error('Failure sending notification to', tokens[index], error);
//        // Cleanup the tokens who are not registered anymore.
//        if (error.code === 'messaging/invalid-registration-token' || error.code === 'messaging/registration-token-not-registered') {
//          tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
//        }
//      }
//    });
//    return Promise.all(tokensToRemove);
//  });


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
